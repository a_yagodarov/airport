0. Аутентификация -
  После авторизации добавлять хедер
  Authorization : Bearer + полученный токен при авторизации

1. Регистрация - URI : ** /api/phone/v1/register **
    POST data : phone,password
    success : 200 ok, true
    failure : 403 Forbidden, array[Ошибка1, Ошибка2, ... ъ

2. Авторизация - URI : ** /api/phone/v1/login **
    POST data : phone, password, push_token
    success : 200 ok, true
    failure : 403 Forbidden

3. Восстановление пароля -
    3.1 Получение токена URI : ** /api/phone/v1/get_token **
        POST data : phone
    3.2
        Изменение пароля URI : ** /api/phone/v1/change_password **
        data : phone, password, token

4. Аукционы URL :
    4.1 Получить список - GET ** /api/phone/v1/auctions **
    4.2 Сделать ставку - POST ** ** /api/phone/v1/bid_in_auction **
      data : auction_id, km_price, total_price
    4.3 Получить инфо об аукционе - GET ** /api/phone/v1/auction/{id} **
