@echo off
echo.
del *bundle*
call ng build --aot --environment=prod --output-hashing=all --sourcemaps=false --extract-css=true --named-chunks=false --build-optimizer=true
ROBOCOPY dist . /s /e /it
rmdir /s /q dist

GOTO DONE

:UNKNOWN
Echo Error no commit comment
goto:eof

:DONE
ECHO Done!
goto:eof