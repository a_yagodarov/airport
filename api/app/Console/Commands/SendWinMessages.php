<?php

namespace App\Console\Commands;

use App\Models\Phone\Auction;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class SendWinMessages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sendWinMessages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $auctions = Auction
          ::where([
            ['status', '=', '1'],
            ['created_at', '<', Carbon::now()->subMinutes(5)->toDateTimeString()]
          ])->get();
//      var_dump($auctions);
      foreach ($auctions as $auction) {
        $auction->chooseWinner();
      }
    }
}
