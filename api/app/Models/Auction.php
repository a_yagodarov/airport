<?php

namespace App\Http\Models;

use App\Http\Models\GMaps\DistanceMatrix;
use App\Http\Models\Parking\Parking;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class Auction extends Model
{
  protected $table = 'auction';
//  public $timestamps = false;
  protected $fillable = [
    'address',
    'comment',
    'number_of_passengers',
    'client_phone_number',

    'baby_chair',
    'bank_card_payment',
    'external_baggage',
    'report_documents',

    'car_id',
    'driver_id',

    'status',
  ];

  public static function boot()
  {
    parent::boot();

    self::created(function (Auction $model) {

    });
  }

  public function rules()
  {
    return [
      'address' => ['required', 'min:3', 'max:64', function($attribute, $value, $fail) {
        $length = (new DistanceMatrix([
          'origin' => 'Уфа, международный аэропорт',
          'destinations' => $value,
        ]))->getResult();
        if ($length) {
          if (!isset($length->rows['0']->elements[0]->distance)) {
            return $fail('Не удалось проложить маршрут');
          }
        }
      }],
      'comment' => '',
      'number_of_passengers' => 'required|numeric'
    ];
  }

  public static function search(Request $request)
  {
    $models = Auction::
    when($request->get('orderBy'), function ($models) use ($request) {
      return $models
        ->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
    })
      ->when(!$request->get('orderBy'), function ($models) use ($request) {
        return $models
          ->orderBy('car_brands.id', 'desc');
      })
      ->groupBy('id');

    $count = $models->get()->count();
    $models = $models
      ->when($request->get('page') >= 0 && !$request->get('all'), function ($models) use ($request) {
        return $models->skip($request->get('page') * 10)->take(10);
      })
      ->get();
    return response()->json([
      'models' => $models,
      'count' => $count,
      'time_to_live' => 60
    ]);
  }

  public function store(Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules($request), $this->messages());

    if ($validator->fails()) {
      return response()->json($validator->messages(), 403);
    } else {
      $this->fill($request->all());
      $this->notifyDrivers();
      if ($result = $this->save()) {
        return response()->json($this, 200);
      } else
        return response()->json($result, 403);
    }
  }

  public function notifyDrivers()
  {
    $parkings = Parking
      ::leftJoin('phone_register_requests as prr', 'prr.driver_id', '=', 'parking.driver_id')
      ->leftJoin('user_driver as ud', 'ud.driver_id', '=', 'parking.driver_id')
      ->leftJoin('users as u', 'u.id', '=', 'ud.user_id')
      ->leftJoin('cars', 'cars.id', '=', 'parking.car_id')
      ->where('cars.number_of_passengers', '>=', $this->number_of_passengers)
      ->where('cars.baby_chair', '>=', $this->baby_chair)
      ->where('cars.bank_card_payment', '>=', $this->bank_card_payment)
      ->where('cars.external_baggage', '>=', $this->external_baggage)
      ->where('cars.report_documents', '>=', $this->report_documents)
      ->where('prr.status', '=', '1')
      ->get();
    foreach ($parkings as $parking) {
      (new PushNotification())->push($parking->push_token, 'Новый аукцион', 'Аэропорт УФА', [
        'id' => $this->id,
        'address' => $this->address,
        'comment' => $this->comment,
        'action' => '3'
      ]);
    }
    $parkings = Parking
      ::leftJoin('phone_register_requests as prr', 'prr.driver_id', '=', 'parking.driver_id')
      ->leftJoin('user_driver as ud', 'ud.driver_id', '=', 'parking.driver_id')
      ->leftJoin('users as u', 'u.id', '=', 'ud.user_id')
      ->leftJoin('cars', 'cars.id', '=', 'parking.car_id')
      ->orWhere('cars.number_of_passengers', '<', $this->number_of_passengers)
      ->orWhere('cars.baby_chair', '<', $this->baby_chair)
      ->orWhere('cars.bank_card_payment', '<', $this->bank_card_payment)
      ->orWhere('cars.external_baggage', '<', $this->external_baggage)
      ->orWhere('cars.report_documents', '<', $this->report_documents)
      ->orWhere('prr.status', '=', '1')
      ->get();
    foreach ($parkings as $parking) {
      $message = 'В проводимом аукционе Вы не принимаете участие по причине '.$this->requirements($parking);
      (new PushNotification())->push(
        $parking->push_token,
        $message,
        'Аэропорт УФА',
        [
          'id' => $this->id,
          'address' => $this->address,
          'comment' => $this->comment
        ]
      );
    }
    return true;
  }

  public function participants()
  {
    return $this->hasMany('App\Http\Models\AuctionRequest');
  }

  public function requirements($parking)
  {
    $array = [
      'number_of_passengers' => 'требуемых пассажирских мест',
      'bank_card_payment' => 'оплаты банковской картой',
      'baby_chair' => 'детского сиденья',
      'external_baggage' => 'доп.багажника на крыше',
      'report_documents' => 'отчетных документов',
    ];
    $result = 'отсутствия ';
    foreach ($array as $item => $value) {
      if (($this->$item) && !($parking->$item)) {
        $result .= $value;
        $result .= ', ';
      }
    }
    $result = substr($result, 0, -2);
    $result .= '.';
    return $result;
  }

  public function messages()
  {
    return [
      'required' => 'Заполните это поле',
      'min' => 'Не менее :min символа(-ов)',
      'max' => 'Не более :max символа(-ов)',
      'unique' => 'Уже используется',
      'email' => 'Введите правильный формат email',
    ];
  }

  public function driver()
  {
    $this->belongsTo('App\Http\Models\Driver');
  }

  public function car()
  {
    $this->belongsTo('App\Http\Models\Car');
  }
}
