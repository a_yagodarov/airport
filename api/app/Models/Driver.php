<?php

namespace App\Http\Models;

use Illuminate\Support\Facades\App;
use Faker\Provider\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Storage;
class Driver extends Model
{
	protected static function boot()
	{
	    parent::boot();

		static::created(function (Driver $model) {
      if (Auth::user() && Auth::user()->id) {
        $report = new Action();
        $report->action_id = 7;
        $report->model_id = 3;
        $report->driver_id = $model->id;
        $report->by_user_id = Auth::user()->id;
        $report->save();
        }
    });


    static::updated(function (Driver $model) {
      if (Auth::user() && Auth::user()->id) {
        $report = new Action();
        $report->action_id = 8;
        $report->model_id = 3;
        $report->driver_id = $model->id;
        $report->by_user_id = Auth::user()->id;
        $report->save();
      }
    });

	    static::deleted(function (Driver $model) {

	        File::deleteDirectory($model->path());
	    });
	}

	public function path()
	{
		return public_path().'/images/drivers/'.$this->id.'/';
	}

	protected $fillable = [
        'id', 'first_name', 'surname', 'patronymic', 'town_id', 'serial_number', 'experience', 'phone_number',
        'phone_number_2', 'email', 'comment', 'raiting', 'license', 'image', 'passport_number', 'driver_card_serial_number',
        'reviews_text', 'mvd_review_data', 'entry_to_parking', 'terminal_countryside_order_priority', 'sms_code'
    ];

    public function rules(Request $request)
    {
    	if ($request->isMethod('post')) {
    		return [
	    		'entry_to_parking' => '',
	    		'raiting' => ['required', 'numeric', function($attribute, $value, $fail){
    		    if (!($value >= 1 && $value <= 5))
            {
              $fail('Введите число от 1 до 5');
            }
          }],
	    		'first_name' => 'required',
	    		'phone_number' => 'required|unique:drivers,phone_number'
	    	];
    	}
    	else
    	{
    		return [
	    		'entry_to_parking' => '',
          'raiting' => ['required', 'numeric', function($attribute, $value, $fail){
            if (!($value >= 1 && $value <= 5))
            {
              $fail('Введите число от 1 до 5');
            }
          }],
	    		'first_name' => 'required',
	    		'phone_number' => 'required|unique:drivers,phone_number,'.$request->get('id')
	    	];
    	}
    }

    public static function search(Request $request)
	{
		$models =  DB::table('drivers')
			->select('drivers.*')
			->when($request->get('car_id'), function($models) use ($request){
				return $models
				->leftJoin('driver_cars', 'driver_cars.driver_id', '=', 'drivers.id')
				->where('driver_cars.car_id', '=', $request->get('car_id'));
			})
			->when($request->get('first_name'), function($models) use ($request){
				return $models->where('drivers.first_name', 'LIKE', "%{$request->get('first_name')}%");
			})
			->when($request->get('surname'), function($models) use ($request){
				return $models->where('drivers.surname', 'LIKE', "%{$request->get('surname')}%");
			})
			->when($request->get('patronymic'), function($models) use ($request){
				return $models->where('drivers.patronymic', 'LIKE', "%{$request->get('patronymic')}%");
			})
			->when($request->get('phone_number'), function($models) use ($request){
				return $models->where('drivers.phone_number', 'LIKE', "%{$request->get('phone_number')}%");
			})
			->when($request->get('driver_card_serial_number'), function($models) use ($request){
				return $models->where('drivers.driver_card_serial_number', 'LIKE', "%{$request->get('driver_card_serial_number')}%");
			})
			->when($request->get('reviews_text'), function($models) use ($request){
				return $models->where('drivers.reviews_text', 'LIKE', "%{$request->get('reviews_text')}%");
			})
			->when($request->get('comment'), function($models) use ($request){
				return $models->where('drivers.comment', 'LIKE', "%{$request->get('comment')}%");
			})
			->when($request->get('mvd_review_data'), function($models) use ($request){
				return $models->where('drivers.mvd_review_data', 'LIKE', "%{$request->get('mvd_review_data')}%");
			})
			->when($request->get('passport_number'), function($models) use ($request){
				return $models->where('drivers.passport_number', 'LIKE', "%{$request->get('passport_number')}%");
			})
			->when($request->get('experience'), function($models) use ($request){
				return $models->where('drivers.experience', '=', $request->get('experience'));
			})
			->when($request->get('orderBy'), function ($models) use ($request) {
				return $models
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->when(!$request->get('orderBy'), function ($models) use ($request) {
				return $models
					->orderBy('drivers.id', 'desc');
			})
			->groupBy('id');

		$count = $models->get()->count();
		$models = $models

			->when($request->get('page') >= 0 && !$request->get('all'), function ($models) use ($request){
				return $models->skip($request->get('page') * 10)->take(10);
			})
			->get();
		return response()->json([
			'models' => $models,
			'count' => $count
		]);
	}

    public function store(Request $request)
	{
		$validate = Validator::make(Input::all(), $this->rules($request), $this->messages());
		if (!$validate->fails())
		{
			$this->fill($request->all());
			if ($result = $this->save())
			{
				$this->saveFile($request);
				return response()->json($this, 200);
			}
			else
			{
				return response()->json($result, 403);
			}
		}
		else
		{
			return response()->json($validate->errors(), 403);
		}
	}

	public function storeUpdate(Request $request)
	{
		$validate = Validator::make($request->all(), $this->rules($request), $this->messages());
		if (!$validate->fails())
		{
			$this->fill($request->all());
			if ($result = $this->save())
			{
				$this->saveFile($request);
				return response()->json($result, 200);
			}
			else
			{
				return response()->json($result, 403);
			}
		}
		else
		{
			return response()->json($validate->errors(), 403);
		}
	}

	public function saveFile($request)
	{
		$path = public_path().'/images/drivers/'.$this->id.'/';
		if (!File::exists($path)) {
			File::makeDirectory($path, $mode = 0777, true, true);
		}
		if ($request->get('file'))
		{
			$file = @\Intervention\Image\Facades\Image::make($request->get('file'));
			if ($file)
			{
				if (File::exists($path.$this->image))
				{
					File::delete($path.$this->image);
                    $this->image = $this->id.'_'.time().'.jpg';
				};
				$file->resize(640, null, function ($constraint) {
				    $constraint->aspectRatio();
				})->save($path.$this->image);
				$this->save();
			}
		}
	}

    public function saveImageFromUrl($url)
    {
        $path = public_path().'/images/drivers/'.$this->id.'/';
        $newfile = $path.$this->image;
        if (File::exists($path))
        {
            File::delete($path);
        }
        else {
            File::makeDirectory($path, $mode = 0777, true, true);
        }
        if ( copy($url, $newfile) ) {
            return true;
        }else{
            return false;
        }
    }

	public function getFullName()
	{
		return implode(' ', [
			$this->first_name,
			$this->surname,
			$this->patronymic
		]);
	}

	public function messages() {
		return [
			'required' => 'Заполните это поле',
			'min' => 'Не менее :min символа(-ов)',
			'max' => 'Не более :max символа(-ов)',
			'unique' => 'Уже используется',
			'email' => 'Введите правильный формат email',
		];
	}


  public function users()
  {
    return $this->belongsToMany('App\Http\Models\User\User', 'user_driver');
  }
}
