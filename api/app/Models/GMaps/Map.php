<?php

namespace App\Http\Models\GMaps;

use Illuminate\Database\Eloquent\Model;

class Map extends Model
{
    public static function route(array $params)
    {
    	return DistanceMatrix::distanseMatrix($params);
    }

    public static function geocode(array $params)
    {
    	return Geocoder::geocode($params);
    }
}
