<?php

namespace App\Http\Models\GMaps;

use Illuminate\Database\Eloquent\Model;

class Geocoder extends Model
{
	public $defaultParams = [];
	public $params;
	public $output;

	public function __construct(array $params)
	{
		$this->params = $params;
		if (!$result = @$this->getResult())
		{
//			abort(403, "Can't get result");
		}
		// abort(403, $params['address']);
	}

    public function initCurl()
    {
    	$ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $this->getUrl());

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // $output contains the output string
        $curl_result = curl_exec($ch);
        if ($curl_result === false) {
            $err = curl_error($ch);
        }

        $output = json_decode($curl_result);

        return $output;
    }

    public function getCoords()
    {
        $result = $this->getResult();
        $return = $result->results[0]->geometry->location;
        return $return;
    }

    public function getResult()
    {
    	$output = $this->initCurl();
    	if ($output && $output->status == 'OK')
        {
        	$this->output = $output;
        	return $output;
        }
        else
        {
        	return false;
        }
    }


    public function getUrl()
    {
    	$url = "https://maps.googleapis.com/maps/api/geocode/json?address=".str_replace(' ', '+', $this->params['address']);
    	$url .= "&key=".config('maps.googleMapsKey')."&language=ru";
    	return $url;
    }
}
