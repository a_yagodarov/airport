<?php

namespace App\Http\Models\Parking;

use App\Http\Models\Action;
use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Models\DriverCar;
use App\Http\Models\Report;
use App\Http\Models\Tariff;
class ParkingMifare extends Parking
{
    public static function search(Request $request)
    {
        $models =  Parking::
        with('car')
            ->with('car.carBrand')
            ->with('car.brand')
            ->with(['driver' => function($models){
                $models->orderBy('drivers.raiting', 'desc');
            }])
            ->with('brand')
            ->when($request->get('orderBy'), function ($models) use ($request) {
                return $models
                    ->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
            })
            ->orderBy('parking.town_center', 'asc')
            ->groupBy('id');

        $count = $models->get()->count();
        $models = $models
            ->get();
        return response()->json([
            'models' => $models,
            'count' => $count
        ]);
    }

	protected static function boot()
    {
        static::created(function (ParkingMifare $model) {
          $report = new Action();
          $report->action_id = 11;
          $report->model_id = 3;
          $report->car_id = $model->car_id;
          $report->by_user_id = \Auth::user()->id;
          $report->save();
        });
    }

    public function getFullInfo(Request $request)
    {
        $driverCar = DriverCar::where('mifare_card_id', $request->get('id'))->first();
        if (!$driverCar)
        {
            return response()->json(false, 403);
        };
        $car = $driverCar->car;
        $driver = $driverCar->driver;
        $result = new \stdClass();
        if ($car->use_group_tariff && $car->group_tariff_id)
        {
            $tariff = Tariff::where('id', $car->group_tariff_id)->first();
            $result->town = $tariff->town;
            $result->town_center = $tariff->town_center;
            $result->periferia = $tariff->periphery;
            $result->km_price = $tariff->km_price;
        }
        else
        {
            $result->town = $driver->town;
            $result->town_center = $driver->town_center;
            $result->periferia = $driver->periphery;
            $result->km_price = $driver->km_price;
        }
        $result->company = $car->carBrand->name;
        $result->model = $car->model;
        $result->color = $car->color;
        $result->number = $car->number;
        $driver->image ? $result->image = url('/').'/public/images/drivers/'.$driver->id.'/'.$driver->image : $result->image = "https://place-hold.it/200x200&text=No_image";
        $result->full_name = $driver->getFullName();
        $result->serial_number = $driver->serial_number;
        return response()->json($result, 200);
    }

    public function addByMifareCardId(Request $request)
    {
        $driverCar = DriverCar::where('mifare_card_id', $request->get('id'))->first();
        if (!$driverCar)
        {
            return response()->json([
                'mifare_card_id' => ['Не найден авто по такому id']
            ], 403);
        }
        $parking = new ParkingMifare();
        $parking->driver_id = $driverCar->driver_id;
        $parking->car_id = $driverCar->car_id;
        $parking->checkIsGroupTariffThenFill();
        $validator = Validator::make($parking->attributes, $this->rules($request), $this->messages());
        if ($validator->fails()) {
            return response()->json(false, 200);
        }
        else
        {
            if (!$parking->driver->entry_to_parking)
            {
                return response()->json(true, 200);
            }
            $parking->save();
        	return response()->json(true, 200);
        }
    }
}
