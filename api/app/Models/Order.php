<?php

namespace App\Http\Models;

use App\Models\Options;
use Illuminate\Http\Request;
use App\Http\Models\Parking\Parking;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Order extends Parking
{
  protected $table = 'orders';
  static $hours_sub_manager = 12;
  static $table_name = "orders";
  public $zone;
  public $sms;

  protected static function boot()
  {

  }

  public function __construct(array $attributes = [])
  {
    $this->table = 'orders';
    parent::__construct($attributes);
  }

  public static function search(Request $request)
  {
    $models = Order::with('car')
      ->with('driver')
      ->with('car.carClass')
      ->with('car.carBrand')
      ->with('car.brand')
      ->where('orders.user_id', '=', Auth::user()->id)
      ->where('orders.created_at', '>', Carbon::now()->subHours(self::$hours_sub_manager))
      ->when($request->get('orderBy'), function ($models) use ($request) {
        return $models
          ->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
      })
      ->when(!$request->get('orderBy'), function ($models) use ($request) {
        return $models
          ->orderBy('orders.id', 'desc');
      })
      ->groupBy('id');

    $count = $models->get()->count();
    $models = $models
      ->get();
    return response()->json([
      'models' => $models,
      'count' => $count
    ]);
  }

  public static function getMostOrdered(Request $request) {
    $limit = $request->get('limit');
      return Order::with('car')
        ->with('car.carBrand')
        ->with('car.carClass')
        ->selectRaw('count(*) as count, car_id, orders.*')
        ->groupBy('car_id')
        ->limit($limit)
        ->when($request->get('date_from'), function($model) use ($request) {
          return $model->where(
            'orders.created_at', '>', $request->get('date_from')
          );
        })
        ->when($request->get('date_to'), function($model) use ($request) {
          return $model->where(
            'orders.created_at', '<', $request->get('date_to')
          );
        })
        ->orderBy('count', 'DESC')
        ->get();
  }

  public static function getOrdersByManager(Request $request) {
    $limit = $request->get('limit');
    $result = DB::select('select
          count(*) as cnt, o.car_id, o.user_id, c.model, u.first_name, u.surname, cb.name,
          c.number
        from orders o 
        left join cars c on c.id = o.car_id 
        left join car_brands cb on cb.id = c.car_brand_id
        inner join (
          select u.id, u.first_name, u.surname from users u
          limit :limit
        ) as u on u.id = o.user_id
        group by user_id, car_id
        order by o.user_id ASC, cnt DESC', ['limit' => $limit]);
    //          left join model_has_roles m on u.id = m.model_id
    //          left join roles r on m.role_id = r.id
    //          where r.name = "manager"
    return $result;
  }

  public function user()
  {
    return $this->belongsTo('App\Http\Models\User\User');
  }

  public function addToParking(Request $request)
  {
    $parking = new Parking();
    $parking->store($request);
    if ($parking->id) {
      return true;
    } else {
      return false;
    }
    // return $parking->save();
  }

  public function sendMessage($options = [])
  {
    $message = $this->getMessage($options);

    $soapClient = new \SoapClient('https://smsc.ru/sys/soap.php?wsdl');
    if ($this->driver && $this->driver->phone_number) {
      $result = $soapClient->send_sms([
        'login' => $this->getSmsAuth()['sms_login'],
        'psw' => $this->getSmsAuth()['sms_password'],
        'phones' => $this->driver->phone_number,
        'mes' => $message,
        'id' => $this->order_id,
        'sender' => '89175566606'
      ]);
      $this->sms = $result;
    }
    return $this->reportSms($result, $message, $this->driver->phone_number);

  }

  public function reportSms($result, $message, $phone)
  {
    if (isset($result->sendresult->error)) {
      $sms = new Sms();
      $sms->status = $result->sendresult->error;
      $sms->message = $message;
      $sms->driver_id = $this->driver->id;
      $sms->send_to = $this->driver->phone_number;
      $sms->save();
    } else {
      $sms = new Sms();
      $sms->status = 0;
      $sms->message = $message;
      $sms->driver_id = $this->driver->id;
      $sms->send_to = $this->driver->phone_number;
      $sms->save();
    }

    return 'succeed';
  }

  public function getSmsAuth()
  {
    return Options::find(1);
  }

  public function getError()
  {
    return [
      1 => 'Ошибка в параметрах.',
      2 => 'Неверный логин или пароль.',
      3 => 'Недостаточно средств на счете Клиента.',
      4 => 'IP-адрес временно заблокирован из-за частых ошибок в запросах.',
      5 => 'Неверный формат даты.',
      6 => 'Сообщение запрещено (по тексту или по имени отправителя).',
      7 => 'Неверный формат номера телефона.',
      8 => 'Сообщение на указанный номер не может быть доставлено.',
      9 => 'Отправка более одного одинакового запроса на передачу SMS-сообщения либо более пяти одинаковых запросов на получение стоимости сообщения в течение минуты.
			Данная ошибка возникает также при попытке отправки более 15 любых запросов одновременно.',
    ];
  }

  public function getMessage($options = [])
  {
    if ((array_key_exists('cancel', $options) && ($options['cancel'] == true))) {
      return 'Заказ отменен';
    } else {
      switch ($this->order_from) {
        case 'parking' :
          return
            'Номер заказа: ' . $this->id .
            ', тариф: ' . $this->getTariffById() .
            ', терминал: ' . $this->getTerminals()[Auth::user()->terminal];
        case 'terminal' :
          return
            'Номер заказа: ' . $this->id .
            ', тариф: ' .
            $this->getTariffByName($this->car->getTariffByAddress($this->address)) .
            ', адрес: ' . $this->address .
            ', стоимость: ' . $this->getPrice() . ' руб.' .
            ', терминал:' . Auth::user()->first_name;
      }
    }
  }

  public function getTerminals()
  {
    return [
      '' => 'Нет',
      1 => 'МВЛ',
      2 => 'ВВЛ',
    ];
  }

  public
  function getPrice()
  {
    $tariff = $this->car->getTariffByAddress($this->address);
    $price = $this->car->getPriceByTariff($this, $this->length, $tariff);
    // abort(403, $price);
    return $price;
  }

  public
  function getTariffById()
  {
    $this->starting_rate ? $starting_rate = 'Посадка ' . $this->starting_rate . ' руб. + ' : $starting_rate = '';
    switch ($this->zone) {
      case 'custom_tariff':
        return $this->custom_tariff;
      case 'town_center':
        return 'Центр ' . $this->town_center;
      case 'town':
        return 'Город ' . $this->town;
      case 'periphery':
        return 'периферия ' . $this->periphery;
      case 'km_price':
        return 'межгород ' . $this->km_price;
      case 'town_km_price':
        return $starting_rate . $this->town_km_price . ' руб/км';
      case 'countryside_km_price':
        return $starting_rate . $this->countryside_km_price . ' руб/км';
    }
  }

  public
  function getTariffByName($name)
  {
    if ($this->type_tariff_use == 1 || !$this->type_tariff_use) {
      switch ($name) {
        case 'custom_tariff':
          return $this->custom_tariff;
        case 'town_center':
          return 'Центр ' . $this->town_center;
        case 'town':
          return 'Город ' . $this->town;
        case 'periphery':
          return 'периферия ' . $this->periphery;
        case 'km_price':
          return 'межгород ' . $this->km_price;
        case 'town_km_price':
          return 'Цена за км(город)' . $this->town_km_price;
        case 'countryside_km_price':
          return 'Цена за км(периферия) ' . $this->countryside_km_price;
      }
    } else {
      switch ($name) {
        case 'town_center':
          return 'Цена за км(город)' . $this->town_km_price;
        case 'town':
          return 'Цена за км(город)' . $this->town_km_price;
        case 'periphery':
          return 'Цена за км(периферия) ' . $this->countryside_km_price;
        case 'km_price':
          return 'Цена за км(периферия) ' . $this->countryside_km_price;
        case 'town_km_price':
          return 'Цена за км(периферия) ' . $this->countryside_km_price;
        case 'countryside_km_price':
          return 'Цена за км(периферия) ' . $this->countryside_km_price;
      }
    }

  }

  protected
    $fillable = [
    'id',
    'driver_id',
    'car_id',
    'town',
    'town_center',
    'km_price',
    'periphery',
    'use_new_tariffs',
    'use_custom_tariff',
    'custom_tariff',
    'type_tariff_use',
    'starting_rate',
    'town_km_price',
    'order_from',
    'countryside_km_price',
    'address',
    'length'
  ];
}
