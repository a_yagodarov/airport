<?php

namespace App\Http\Models;

use Illuminate\Support\Facades\App;
use Faker\Provider\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Storage;

class DriverCar extends Model
{
	protected $fillable = [
        'id', 'driver_id', 'car_id', 'mifare_card_id'
    ];

    public function rules(Request $request)
    {
    	if ($request->isMethod('post')) {
			return [
	    		'car_id' => 'required',
	    		'driver_id' => 'required',
	    		'mifare_card_id' => 'required|unique:driver_cars,mifare_card_id',
	    	];
		}
		elseif ($request->isMethod('put')) {
			return [
	    		'car_id' => 'required',
	    		'driver_id' => 'required',
	    		'mifare_card_id' => 'required|unique:driver_cars,mifare_card_id,'.$request->get('id'),
	    	];
		}
    }

    public static function search(Request $request)
	{
		$models =  DB::table('driver_cars')
			->select('driver_cars.*')
			->when($request->get('driver_id'), function($models) use ($request){
				return $models->select('driver_cars.id', 'driver_cars.mifare_card_id', 'car_brands.name as car_name', 'car_classes.name as class_name', 'cars.model', 'cars.color', 'cars.number')
                    ->where('driver_cars.driver_id', '=', $request->get('driver_id'))
                    ->leftJoin('cars', 'cars.id', '=', 'driver_cars.car_id')
                    ->leftJoin('car_brands', 'car_brands.id', '=', 'cars.car_brand_id')
                    ->leftJoin('car_classes', 'car_classes.id', '=', 'cars.class_id');
			})
			->when($request->get('car_id'), function($models) use ($request){
				return $models->select('driver_cars.id', 'driver_cars.mifare_card_id', DB::raw('CONCAT_WS(" ", drivers.first_name, drivers.surname, drivers.patronymic) as full_name'))
				->leftJoin('drivers', 'drivers.id', '=', 'driver_cars.driver_id')
				->where('driver_cars.car_id', '=', $request->get('car_id'));
			})
			->when($request->get('mifare_card_id'), function($models) use ($request){
				return $models->where('driver_cars.mifare_card_id', '=', $request->get('mifare_card_id'));
			})
			->when($request->get('orderBy'), function ($models) use ($request) {
				return $models
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->when(!$request->get('orderBy'), function ($models) use ($request) {
				return $models
					->orderBy('driver_cars.id', 'desc');
			})
			->groupBy('id');

		$count = $models->get()->count();
		$models = $models

			->when($request->get('page') >= 0 && !$request->get('all'), function ($models) use ($request){
				return $models->skip($request->get('page') * 10)->take(10);
			})
			->get();
		return response()->json([
			'models' => $models,
			'count' => $count
		]);
	}

	public function validate(Request $request)
	{
		return Validator::make($request->all(), $this->rules($request), $this->messages());
	}

    public function store(Request $request)
	{
		$validator = $this->validate($request) ;
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->fill($request->all());
			if ($result = $this->save())
			{
				return response()->json($result, 200);
			}
			else
				return response()->json($result, 403);
		}
	}

	public function storeUpdate(Request $request)
	{
		$validate = Validator::make($request->all(), $this->rules($request), $this->messages());
		if (!$validate->fails())
		{
			$this->fill($request->all());
			if ($result = $this->save())
			{
				return response()->json($result, 200);
			}
			else
			{
				return response()->json($result, 403);
			}
		}
		else
		{
			return response()->json($validate->errors(), 403);
		}
	}

	public function messages() {
		return [
			'required' => 'Заполните это поле',
			'min' => 'Не менее :min символа(-ов)',
			'max' => 'Не более :max символа(-ов)',
			'unique' => 'Уже используется',
			'email' => 'Введите правильный формат email',
		];
	}

    public function driver()
    {
        return $this->belongsTo('App\Http\Models\Driver');
    }

    public function car()
    {
        return $this->belongsTo('App\Http\Models\Car');
    }
}
