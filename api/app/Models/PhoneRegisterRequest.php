<?php

namespace App;

use App\Http\Models\CustomModel;
use App\Http\Models\Driver;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Request;
use Validator;

class PhoneRegisterRequest extends CustomModel
{
  protected $fillable = [
    'phone', 'status'
  ];
    protected $table = 'phone_register_requests';

    public function rules(Request $request)
    {
      return [
        'phone' => ['required', 'min:11', 'max:12', function($attribute, $value, $fail) {
            if (!Driver::where('phone_number', 'LIKE', "%{substr($value, -10)}%"))
              $fail('Неправильный номер');
        }]
      ];
    }

    public function messages()
    {
      return [
        'phone.required' => 'Необходимо заполнить тел.номер'
      ];
    }

  public function store(Request $request)
  {
    $validator = Validator::make($request->all(), $this->rules($request), $this->messages());
    if ($validator->fails()) {
      return response()->json($validator->messages(), 403);
    } else {
      $this->fill($request->all());
      if ($result = $this->save()) {
        $this->saveFile($request);
        return response()->json($this, 200);
      } else
        return response()->json($result, 403);
    }
  }

    public static function search(Request $request)
    {
        $models =  DB::table(with(new self)->getTable())
            ->select('*')
            ->when($request->get('orderBy'), function ($models) use ($request) {
                return $models
                    ->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
            })
            ->when(!$request->get('orderBy'), function ($models) use ($request) {
                return $models
                    ->orderBy('id', 'desc');
            })
            ->groupBy('id');

        $count = $models->get()->count();
        $models = $models
            ->when($request->get('page') >= 0 && !$request->get('all'), function ($models) use ($request){
                return $models->skip($request->get('page') * 10)->take(10);
            })
            ->get();
        return response()->json([
            'models' => $models,
            'count' => $count
        ]);
    }
}
