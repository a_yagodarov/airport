<?php

namespace App\Models\Sms;

use Illuminate\Http\Request;

class Parking extends \App\Http\Models\Parking\Parking
{
  public static function boot()
  {

  }

  public static function exit(Request $request)
  {
    $phone = substr($request->get('phone'), -10);
    \Log::info(json_encode($request));
    $parking = self::whereHas('driver', function ($q) use ($phone) {
      $q->where('phone_number', 'LIKE', "%{$phone}%");
    })->first();
    if ($parking) {
      return response()->json($parking->delete(), 200);
    } else {
      return response()->json($parking, 200);
    }
  }
}
