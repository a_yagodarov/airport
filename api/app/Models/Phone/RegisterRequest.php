<?php

namespace App\Models\Phone;

use App\Http\Models\Driver;
use App\Http\Models\User\User;
use App\Models\UserDriver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

/**
 * @property int $id
 * @property string $sms_login
 * @property string $sms_password
 * @property string $mail_login
 * @property string $mail_password
 * @property int $terminal_timeout
 * @property string $created_at
 * @property string $updated_at
 */
class RegisterRequest extends Model
{
  protected $table = 'phone_register_requests';
  /**
   * @var array
   */


  protected $guard_name = 'api';

  protected $fillable = [
    'phone_number', 'status', 'driver_id', 'password'];

  public static function search(Request $request)
  {
    $models = RegisterRequest::when($request->get('orderBy'), function ($models) use ($request) {
      return $models
        ->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
    })
      ->when(!$request->get('orderBy'), function ($models) use ($request) {
        return $models
          ->orderBy('phone_register_requests.id', 'asc');
      })
      ->with('driver')
      ->groupBy('id');

    $count = $models->get()->count();
    $models = $models
      ->get();
    return response()->json([
      'models' => $models,
      'count' => $count
    ]);
  }

  public function rules(Request $request)
  {
    if ($request->isMethod('post')) {
      return [
        'phone_number' => ['required', 'unique:phone_register_requests,phone_number', 'regex:/[0-9]{10,10}+$/', 'max:64', function ($attribute, $value, $fail) use ($request) {
          $value = substr($value, -9);
          if (!Driver::where('phone_number', 'LIKE', "%{$value}")->first()) {
            return $fail('Водитель с таким номером не зарегестрирован');
          }
        }],
        'password' => 'required|max:64',
      ];
    } else {
      return [
        'phone_number' => ['required', 'unique:phone_register_requests,phone_number,'.$request->get('id'), 'regex:/[0-9]{10,10}+$/', function ($attribute, $value, $fail) use ($request) {
          $value = substr($value, -9);
          if (!Driver::where('phone_number', 'LIKE', "%{$value}")->first()) {
            return $fail('Водитель с таким номером не зарегестрирован');
          }
        },
        'status' => 'required',
        'password' => 'required',
      ]];
    }
  }

  public function store(Request $request)
  {
    $validator = \Validator::make(Input::all(), $this->rules($request), $this->messages());
    if ($validator->fails()) {
      return response()->json($validator->messages()->all(), 403);
    } else {
      if ($request->isMethod('post'))
      {
        /* Реквест от моб.приложения */
        if ($result = $this->save()) {
          $this->fillDriverId($request);
          $this->fillByRequest($request);
          $this->savePassword($request);
          $this->save();
          return response()->json($result, 200);
        } else
          return response()->json($result, 403);
      }

      /* Реквест от сайта */
      if ($request->isMethod('put'))
      {
        $this->updateStatus();
        $this->save();
        $this->registerUser();
      }
    }
  }

  public function updateStatus()
  {
    $this->status = !$this->status;
  }

  public function registerUser()
  {
    $driver = $this->driver;
    if (!$user = User::where('phone_number', 'LIKE', "%".$this->phone_number)->first())
    {
      $user = new User();
      $user->first_name = $driver->first_name;
      $user->surname = $driver->surname;
      $user->patronymic = $driver->patronymic;
      $user->password = $this->password;
      $user->phone_number = $this->phone_number;
      $user->save();
      $user->assignRole('driver');
      $userDriver = new UserDriver();
      $userDriver->user_id = $user->id;
      $userDriver->driver_id = $driver->id;
      $userDriver->save();
    }

  }


  public function fillDriverId($request)
  {
    $value = substr($request->get('phone_number'), -10);
    $driver = Driver::where('phone_number', 'LIKE', "%".$value)->first();
    $this->driver_id = $driver->id;
  }

  public function savePassword($request)
  {
    $this->password = bcrypt($request->password);
  }

  public function fillByRequest($request)
  {
    $this->fill($request->only(['phone_number', 'password']));
  }

  public function driver()
  {
    return $this->belongsTo(Driver::class);
  }

  public function messages()
  {
    return [
      'required' => 'Заполните это поле',
      'phone_number.unique' => 'Вы уже подали зявление на регистрацию по этому номеру',
      'min' => 'Не менее :min символа(-ов)',
      'max' => 'Не более :max символа(-ов)',
      'unique' => 'Уже используется',
      'email' => 'Введите правильный формат email',
    ];
  }
}
