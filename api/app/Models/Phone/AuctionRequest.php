<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 18.11.2018
 * Time: 17:59
 */

namespace App\Models\Phone;


use App\Http\Models\Parking\Parking;
use App\PhoneRegisterRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Models\CustomModel;

class AuctionRequest extends CustomModel
{
  protected $table = 'auction_requests';
  public $timestamps = false;

  protected $fillable = [
    'auction_id',
    'user_id',
    'driver_id',
    'car_id',
    'km_price',
    'total_price',
  ];

  public function rules(Request $request)
  {
    return [
      'auction_id' => ['required', function ($attribute, $value, $fail) {
        $user_id = \Auth::user()->id;
        $parking = Parking
          ::leftJoin('phone_register_requests as prr', 'prr.driver_id', '=', 'parking.driver_id')
          ->leftJoin('user_driver as ud', 'ud.driver_id', '=', 'parking.driver_id')
          ->leftJoin('users as u', 'u.id', '=', 'ud.user_id')
          ->leftJoin('cars', 'cars.id', '=', 'parking.car_id')
          ->where('u.id', '=', $user_id)
          ->orWhere('prr.status', '=', '1')
          ->first();
        if ($parking) {
          $auction = Auction::find($value);
          if (
            $parking->number_of_passengers < $auction->number_of_passengers ||
            $parking->baby_chair < $auction->baby_chair ||
            $parking->bank_card_payment < $auction->bank_card_payment ||
            $parking->external_baggage < $auction->external_baggage ||
            $parking->report_documents < $auction->report_documents
          )
          return $fail("Ваш автомобиль не соответсвтует требованиям клиента");
        } else {
          return $fail("Авто отсутствует на стоянке");
        }
        $auction = Auction::find($value);
        if (!$auction)
          return $fail('Аукцион с таким id не найден');
        $time = time();
        $strtotime = strtotime($auction->created_at);
        if (time() > strtotime($auction->created_at) + 59) {
          return $fail("Время на ставку вышло");
        }
        if (AuctionRequest::where('auction_id', '=', $value)->where('user_id', '=', \Auth::user()->id)->first()) {
          return $fail('Вы уже сделали ставку');
        }

      }],
      'km_price' => ['sometimes', 'numeric'],
      'total_price' => ['numeric', function($attribute, $value, $fail) use ($request) {
        if (!$request->get('km_price') && !$value)
        {
          return $fail('Введите итоговую сумму');
        }
      }],
    ];
  }

  public static function search(\Request $request)
  {
    $models = self::
    when($request->get('orderBy'), function ($models) use ($request) {
      return $models
        ->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
    })
      ->orderBy('auction_requests.id', 'asc')
      ->groupBy('id');

    $count = $models->get()->count();
    $models = $models
      ->get();
    return response()->json([
      'models' => $models,
      'count' => $count
    ]);
  }

  public function store(Request $request)
  {
    $validator = \Validator::make(Input::all(), $this->rules($request), $this->messages());
    if ($validator->fails()) {
      return response()->json($validator->messages()->all(), 403);
    } else {
      $this->fill($request->all());
      $this->fillCarAndUserIds();
      if ($result = $this->save()) {
        return response()->json($result, 200);
      } else
        return response()->json($result, 403);
    }
  }

  public function fillCarAndUserIds()
  {
    $drivers = \Auth::user()->driver;
    $drivers2 = $drivers[0];
    $this->driver_id = $drivers[0]->id;
    $this->user_id = \Auth::user()->id;
  }

  public function messages()
  {
    return [
      'auction_id.required' => 'Не выбран аукцион',
      'km_price.required' => 'Введите цену за км',
      'total_price.required' => 'Введите итоговую сумму',
      'required_if' => 'Заполните это поле',
      'required_without' => 'Заполните это поле',
      'min' => 'Не менее :min символа(-ов)',
      'max' => 'Не более :max символа(-ов)',
      'unique' => 'Уже используется',
      'email' => 'Введите правильный формат email',
    ];
  }
}
