<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 21.11.2018
 * Time: 17:17
 */

namespace App\Models\Phone;


use App\Http\Models\CustomModel;
use App\Http\Models\Driver;
use App\Http\Models\DriverTariff;
use App\Models\Sms\Parking;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class Tariff extends CustomModel
{
  protected $table = 'driver_tariffs';
  protected $fillable = [
    'id', 'town_center', 'town', 'periphery', 'use_custom_tariff', 'custom_tariff', 'km_price', 'type_tariff_use', 'starting_rate', 'town_km_price', 'countryside_km_price'
  ];
  public static function getTariff($driver_id)
  {
    $driverTariff = DriverTariff::where('id', '=', $driver_id)->first();
    if ($driverTariff) {
      return response()->json($driverTariff, 200);
    } else {
      return response()->json([
        "Редактирование групповых тарифов невозможно"
      ], 403);
    }
  }

  public function rules()
  {
    return [
      // 'id' => 'required',
      'town' => 'required_if:type_tariff_use,1',

      'custom_tariff' => 'required_if:use_custom_tariff,1'
    ];
  }

  public function store(Request $request)
  {
    $validator = \Validator::make(Input::all(), $this->rules($request), $this->messages());
    if ($validator->fails()) {
      return response()->json($validator->messages()->all(), 403);
    }
    else
    {
      $this->fill($request->all());
      $this->updateParking();
      if ($result = $this->save())
      {
        return response()->json($result, 200);
      }
      else
        return response()->json($result, 403);
    }
  }

  public function updateParking() {
    if ($parking = Parking::where('driver_id', '=', $this->id)->first()) {
      $parking->fill($this->attributes);
      $parking->save();
    }
  }
}
