<?php

namespace App\Http\Controllers\Mifare\Auth;

use App\Http\Controllers\Controller;
use App\Http\Models\User\User;
use App\Http\Models\User\Profile;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
	public function authenticate(Request $request)
	{
		$validator = Validator::make($request->all(), $this->rules($request), $this->messages());
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$credentials = $request->only('username', 'password');
			try {
			$user = User::where('username', $request->get('username'))->first();
				if (! $token = JWTAuth::attempt($credentials)) {
					return response()->json('Неверные данные', 401);
				}
			} catch (JWTException $e) {
				// something went wrong whilst attempting to encode the token
				return response()->json('Ошибка сервера', 500);
			}

			// all good so return the token
			$permissions = $user->getAllPermissions()->pluck('name');
			$roles = $user->getRoleNames();
			return response()->json([
				'token' => $token, 
				// 'permissions' => $permissions, 
				'roles' => $roles,
				'user' => $user,
			]);
		}
	}

	public function rules(Request $request)
	{
		return [
			'username' => ["required", function($attribute, $value, $fail) use ($request) {
				if ($user = User::where('username', $request->get('username'))->first())
				{
					if (!($user->hasRole('operator') || $user->hasRole('admin')))
					{
						$fail("Выполните вход от оператора или администратора");		
					}
					if ($user->blocked)
					{
						$fail("Пользователь заблокирован");	
					}
				}
				else
				{
					$fail("Пользователь не найден");
				}
			}],
			'password' => ["required", function($attribute, $value, $fail) use ($request) {
				// if (!$user = User::where([
				// 	['username', '=', $request->get('username')],
				// 	['password', '=', bcrypt($request->get('password'))],
				// ])->first())
				$credentials = $request->only('username', 'password');
				if (!Auth::once($credentials))
				{
					$fail("Такая пара логина и пароля не найдена");
				}
			}]
		];
	}

	public function messages() {
		return [
			'required' => 'Заполните это поле',
			'min' => 'Не менее :min символа(-ов)',
			'max' => 'Не более :max символа(-ов)',
			'unique' => 'Уже используется',
			'email' => 'Введите правильный формат email',
		];
	}
}
