<?php

namespace App\Http\Controllers\Mifare;

use Illuminate\Http\Request;
use App\Http\Models\Shift as BaseModel;

class ShiftController extends Controller
{
    public function open(Request $request)
    {
        return (new BaseModel())->start($request);
    }

    public function close(Request $request)
    {
        return (new BaseModel())->close($request);
    }
}
