<?php

namespace App\Http\Controllers;

use App\Http\Models\Brand;
use App\Http\Models\CarBrand;
use App\Http\Models\CarClass;
use App\Http\Models\ClientType;
use App\Http\Models\Tariff;
use Illuminate\Http\Request;
use App\Http\Models\Car as BaseModel;

class CarController extends Controller
{
    public function index(Request $request)
    {
		return BaseModel::search($request);
    }

    public function orderSearch(Request $request)
    {
        return (new BaseModel())->orderSearch($request);
    }

	public function get($id)
	{
		$model = BaseModel::find($id);
		return response()->json($model);
	}

    public function getParams()
    {
        $taxiBrands = Brand::all();
        $carBrands = CarBrand::all();
        $clientTypes = ClientType::all();
        $carClasses = CarClass::all();
        $groupTariffs = Tariff::all();
        $return = [
            'taxi_brands' => $taxiBrands,
            'car_brands' => $carBrands,
            'client_types' => $clientTypes,
            'car_classes' => $carClasses,
            'group_tariffs' => $groupTariffs,
        ];
        return response()->json($return);
    }

    public function blackList(Request $request)
    {
        $car = BaseModel::find($request->get('id'));
        $car->is_black_list = !$car->is_black_list;
        return response()->json($car->save());
    }

    public function permission(Request $request)
    {
        $car = BaseModel::find($request->get('id'));
        $car->permission_to_drive = !$car->permission_to_drive;
        return response()->json($car->save());
    }

    public function store(Request $request)
    {
	    return (new BaseModel())->store($request);
    }

    public function update(Request $request)
    {
        $model = BaseModel::find($request->get('id'));
	    return ($model->storeUpdate($request));
    }

    public function delete($id)
    {
	    return response()->json(BaseModel::find($id)->delete());
    }
}
