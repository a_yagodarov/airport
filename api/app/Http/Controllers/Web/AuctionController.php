<?php

namespace App\Http\Controllers;

use App\Http\Models\Auction;
use App\Http\Models\GMaps\DistanceMatrix;
use App\Jobs\AuctionDelay;
use Illuminate\Http\Request;
use App\Http\Models\Auction as BaseModel;

class AuctionController extends Controller
{
  public function index(Request $request)
  {
    return BaseModel::search($request);
  }

  public function get($id)
  {
    $model = BaseModel::find($id);
    return response()->json($model);
  }

  public function getParticipants($id)
  {
    $model = BaseModel::find($id);
    $model->participants;
    foreach ($model->participants as $participant) {
      $d = $participant->driver;
    }
    return response()->json($model);
  }

  public function store(Request $request)
  {
    return (new BaseModel())->store($request);
  }

  public function chooseWinner(Request $request)
  {
    $auction = Auction::where('id', '=', $request->get('id'))->first();
    $this->dispatch((new AuctionDelay($auction))->delay(60));
  }

  public function update(Request $request)
  {
    $model = BaseModel::find($request->get('id'));
    return ($model->storeUpdate($request));
  }

  public function delete($id)
  {
    return response()->json(BaseModel::find($id)->delete());
  }
}
