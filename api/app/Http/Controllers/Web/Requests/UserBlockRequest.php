<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
class UserBlockRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required', function($attribute, $value, $fail){
                if (Auth::user()->id == $this->id) {
                        return $fail("Нельзя заблокировать себя");
                    }
            }],
            'blocked' => 'required',
        ];
    }
}
