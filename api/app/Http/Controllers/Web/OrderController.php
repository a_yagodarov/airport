<?php

namespace App\Http\Controllers;

use App\Http\Models\Order;
use Illuminate\Http\Request;
use App\Http\Models\Order as BaseModel;
class OrderController extends Controller
{
    public function index(Request $request)
    {
		return BaseModel::search($request);
    }

    public function sendSms(Request $request)
    {
        $model = BaseModel::find($request->get('id'));
        $model->zone = $request->get('zone');
        if ($result = $model->sendMessage() == 'succeed')
        {
            return response()->json(true, 200);
        }
        else
        {
            return response()->json($result, 403);
        }
    }

    public function addToParking(Request $request)
    {
        $model = BaseModel::find($request->get('id'));
        if ($model->addToParking($request))
        {
            $model->sendMessage([
                'cancel' => true
            ]);
            $model->delete();
            return response()->json(true, 200);
        }
        else
        {
            return response()->json(false, 403);
        }
    }

    public function getMostOrdered(Request $request)
    {
      return Order::getMostOrdered($request);
    }

    public function getOrdersByUser(Request $request)
    {
      return Order::getOrdersByManager($request);
    }

	public function get($id)
	{
		$model = BaseModel::with('driver')
            ->with('car.carClass')
            ->with('car.carBrand')
            ->with('car.brand')
            ->find($id);
		return response()->json($model);
	}

    public function store(Request $request)
    {
	    return (new BaseModel())->store($request);
    }

    public function update(Request $request)
    {
        $model = BaseModel::find($request->get('id'));
	    return ($model->storeUpdate($request));
    }

    public function delete($id)
    {
	    return response()->json(BaseModel::find($id)->delete());
    }
}
