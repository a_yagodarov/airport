<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Options as BaseModel;
class OptionsController extends Controller
{
    public function index(Request $request)
    {
        return BaseModel::find(1);
    }

    public function get($id)
    {
        $model = BaseModel::find(1);
        return response()->json($model);
    }

    public function store(Request $request)
    {
        return (new BaseModel())->store($request);
    }

    public function update(Request $request)
    {
        $model = BaseModel::find(1);
        return ($model->storeUpdate($request));
    }

    public function delete($id)
    {
        return response()->json(BaseModel::find($id)->delete());
    }
}
