<?php

namespace App\Http\Controllers\Phone\v1;

use App\Http\Controllers\Controller;
use App\Http\Models\Parking\Parking;
use Illuminate\Http\Request;
use App\Models\Phone\PhoneRegister;
use App\Models\Phone\RegisterRequest;

class ParkingController extends Controller
{
  public function exit(Request $request)
  {
    $parking = Parking::where('driver_id', '=', $request->get('id'))->first();
    if ($parking)
    {
      return response()->json($parking->delete(), 200);
    } else
    {
      return response()->json([
        'Не найден на стоянке'
      ], 403);
    }
  }


}
