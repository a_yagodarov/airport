<?php

namespace App\Http\Controllers\Phone\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Phone\PhoneRegister;
use App\Models\Phone\RegisterRequest;

class RegistrationController extends Controller
{
    public function register(Request $request)
    {
      $phoneRegister = new RegisterRequest();
      return $phoneRegister->store($request);
    }

    public function getToken(Request $request)
    {
      $phoneRegister = new PhoneRegister();
      return $phoneRegister->getToken($request);
    }

    public function changePassword(Request $request)
    {
      $phoneRegister = new PhoneRegister();
      return $phoneRegister->changePassword($request);
    }

}
