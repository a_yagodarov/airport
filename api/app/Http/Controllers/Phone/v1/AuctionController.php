<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 19.11.2018
 * Time: 20:37
 */

namespace App\Http\Controllers\Phone\v1;


use App\Http\Controllers\Controller;
use App\Models\Phone\Auction;
use Illuminate\Http\Request;

class AuctionController extends Controller
{
  public function index(Request $request)
  {
    return Auction::search($request);
  }
  public function get($id)
  {
    if ($result = Auction::find($id)) {
      return $result;
    } else {
      return response()->json([
        'Не найден аукцион'
      ], 403);
    }
  }

  public function getWon(Request $request) {
    return Auction::getWonByUser($request);
  }
}
