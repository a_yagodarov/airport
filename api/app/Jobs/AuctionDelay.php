<?php

namespace App\Jobs;

use App\Http\Models\AuctionDriver;
use App\Http\Models\AuctionRequest;
use App\Http\Models\Driver;
use App\Http\Models\GMaps\DistanceMatrix;
use App\Http\Models\PushNotification;
use App\Http\Models\User\User;
use App\Models\Options;
use App\Models\UserDriver;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class AuctionDelay implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $auction;
    public $winner;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($auction)
    {
        $this->auction = $auction;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $this->chooseWinner();
    }

  public function chooseWinner()
  {
    $length = (new DistanceMatrix([
        'origin' => 'Уфа, международный аэропорт',
        'destinations' => $this->auction->address,
      ]))->getResult();
    if ($length) {
        $length = ($length->rows['0']->elements[0]->distance->value) / 1000;

        $auction_requests = AuctionRequest
          ::where('auction_id', '=', $this->auction->id)
          ->get();
        $lowestPrice = 99999999999;
        $winner = null;
        foreach ($auction_requests as $auction_request) {
          if ($auction_request->km_price && $auction_request->total_price) {
            $price = $auction_request->km_price * $length >= $auction_request->total_price ?
              $auction_request->km_price * $length : $auction_request->total_price;
            if ($price < $lowestPrice) {
              $lowestPrice = $price;
              $winner = $auction_request;
            }
          }
        }
        $this->auction->status = 2;
        $this->auction->save();
        $this->winner = $winner;
        $this->sendMessages();
      } else {
      $this->auction->status = 2;
      $this->auction->save();
    }
    }

    public function sendMessages()
    {
      $this->pushMessageWinner();
      $this->pushMessageLoser();
    }

    public function setAuctionWinner($driver)
    {
      $auctionDriver = new AuctionDriver();
      $auctionDriver->driver_id = $driver->id;
      $auctionDriver->auction_id = $this->auction->id;
      $auctionDriver->save();
    }

    public function pushMessageWinner()
    {
      $auctionRequest = $this->winner;


      if ($auctionRequest)
      {
        $this->sendWinnerNotification($auctionRequest);
      }

      if ($auctionRequest) {
        $this->sendLoserNotification($auctionRequest);
      }
      if ($auctionRequest) {
        $userDriver = UserDriver::where('user_id', '=', $auctionRequest->user_id)->first();
        $driver = Driver::where('id', '=', $userDriver->driver_id)->first();
        $this->setAuctionWinner($driver);

        if ($phone = $this->auction->client_phone_number) {
          $soapClient = new \SoapClient('https://smsc.ru/sys/soap.php?wsdl');
          $result = $soapClient->send_sms([
            'login' =>  Options::find(1)['sms_login'],
            'psw' =>  Options::find(1)['sms_password'],
            'phones' => $driver->phone_number,
            'mes' => "Номер телефона клиента : ".$phone,
//          'id' => $this->auction->,
            'sender' => '89175566606'
          ]);
          $result = $soapClient->send_sms([
            'login' =>  Options::find(1)['sms_login'],
            'psw' =>  Options::find(1)['sms_password'],
            'phones' => $phone,
            'mes' => "Номер телефона водителя : ".$driver->phone_number,
//          'id' => $this->order_id,
            'sender' => '89175566606'
          ]);
        }
      }

    }

    public function sendWinnerNotification($auctionRequest)
    {
      $user = User::find($auctionRequest->user_id);
      $message = 'Вы выиграли в аукционе';
      (new PushNotification())->push(
        $user->push_token,
        $message,
        'Аэропорт УФА',
        [
          'id' => $auctionRequest->auction_id,
          'action' => '1'
        ]
      );
    }

    public function sendLoserNotification($auctionRequest)
    {
      $auctionLoosers = AuctionRequest
        ::where('auction_id', '=', $auctionRequest->auction_id)
        ->where('user_id', '<>', $auctionRequest->user_id)
        ->get();
      if ($auctionLoosers) {
        foreach ($auctionLoosers as $item) {
          $user = User::find($item->user_id);
          if ($user) {
            $message = 'Аукцион завершен';
            (new PushNotification())->push(
              $user->push_token,
              $message,
              'Аэропорт УФА',
              [
                'id' => $auctionRequest->auction_id,
                'action' => '2'
              ]
            );
          }
        }
      }
    }

    public function pushMessageLoser()
    {

    }
//  }
}
