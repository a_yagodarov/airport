<?php
Route::group([
  // 'namespace'=>'api',
  // 'domain'=>'api.example.com',
  'prefix' => 'v1',
  // 'middleware'=>['jwt.auth'],
  // 'where'=>['apikey'=>'[a-z0-9]{32}','id'=>'[0-9]+']],
],
  function () {
    Route::post('register', 'Phone\v1\RegistrationController@register');
    Route::post('login', 'Phone\v1\LoginController@login');
    Route::post('get_token', 'Phone\v1\RegistrationController@getToken');
    Route::post('change_password', 'Phone\v1\RegistrationController@changePassword');
    Route::post('bid_in_auction', 'Phone\v1\AuctionRequestController@bid')->middleware(['jwt.auth', 'permission:bid_in_auction']);
    Route::get('auctions', 'Phone\v1\AuctionController@index')->middleware(['jwt.auth']);
    Route::get('auctions_won', 'Phone\v1\AuctionController@getWon')->middleware(['jwt.auth']);
    Route::get('auction/{id}', 'Phone\v1\AuctionController@get')->middleware(['jwt.auth']);
    Route::get('tariff', 'Phone\v1\TariffController@get')->middleware(['jwt.auth']);
    Route::post('tariff', 'Phone\v1\TariffController@update')->middleware(['jwt.auth']);
    Route::post('parking/exit', 'Phone\v1\ParkingController@exit')->middleware(['jwt.auth']);
  });
