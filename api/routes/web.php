<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::post('authenticate', 'Auth\LoginController@authenticate');
// Route::get('parking/get-info', 'ParkingController@getFullInfo');
// Route::get('parking/add', 'ParkingController@addByMifareCardId');

// Route::get('actions', 'ActionsController@index')->middleware(['jwt.auth', 'permission:manage_actions']);
// Route::post('action', 'ActionsController@store')->middleware(['jwt.auth', 'permission:manage_actions']);
// Route::get('action/{id}', 'ActionsController@get')->middleware(['jwt.auth', 'permission:manage_actions']);
// Route::put('action/{id}', 'ActionsController@update')->middleware(['jwt.auth', 'permission:manage_actions']);
// Route::delete('action/{id}', 'ActionsController@delete')->middleware(['jwt.auth', 'permission:manage_actions']);

// Route::get('brands', 'BrandController@index')->middleware(['jwt.auth', 'permission:manage_brands']);
// Route::post('brand', 'BrandController@store')->middleware(['jwt.auth', 'permission:manage_brands']);
// Route::get('brand/{id}', 'BrandController@get')->middleware(['jwt.auth', 'permission:manage_brands']);
// Route::put('brand/{id}', 'BrandController@update')->middleware(['jwt.auth', 'permission:manage_brands']);
// Route::delete('brand/{id}', 'BrandController@delete')->middleware(['jwt.auth', 'permission:manage_brands']);

// Route::get('cars', 'CarController@index')->middleware(['jwt.auth', 'permission:get_cars']);
// Route::get('car_params', 'CarController@getParams')->middleware(['jwt.auth', 'permission:manage_cars']);
// Route::post('car_black_list', 'CarController@blackList')->middleware(['jwt.auth', 'permission:manage_cars']);
// Route::post('car_permission', 'CarController@permission')->middleware(['jwt.auth', 'permission:manage_cars']);
// Route::post('car', 'CarController@store')->middleware(['jwt.auth', 'permission:manage_cars']);
// Route::get('car/{id}', 'CarController@get')->middleware(['jwt.auth', 'permission:manage_cars']);
// Route::put('car/{id}', 'CarController@update')->middleware(['jwt.auth', 'permission:manage_cars']);
// Route::delete('car/{id}', 'CarController@delete')->middleware(['jwt.auth', 'permission:manage_cars']);

// Route::get('car_images', 'CarImageController@index')->middleware(['jwt.auth', 'permission:manage_car_images']);
// Route::post('car_image', 'CarImageController@store')->middleware(['jwt.auth', 'permission:manage_car_images']);
// Route::get('car_image/{id}', 'CarImageController@get')->middleware(['jwt.auth', 'permission:manage_car_images']);
// Route::put('car_image/{id}', 'CarImageController@update')->middleware(['jwt.auth', 'permission:manage_car_images']);
// Route::delete('car_image/{id}', 'CarImageController@delete')->middleware(['jwt.auth', 'permission:manage_car_images']);

// Route::get('car_brands', 'CarBrandController@index')->middleware(['jwt.auth', 'permission:manage_car_brands']);
// Route::post('car_brand', 'CarBrandController@store')->middleware(['jwt.auth', 'permission:manage_car_brands']);
// Route::get('car_brand/{id}', 'CarBrandController@get')->middleware(['jwt.auth', 'permission:manage_car_brands']);
// Route::put('car_brand/{id}', 'CarBrandController@update')->middleware(['jwt.auth', 'permission:manage_car_brands']);
// Route::delete('car_brand/{id}', 'CarBrandController@delete')->middleware(['jwt.auth', 'permission:manage_car_brands']);

// Route::get('client_types', 'ClientTypeController@index')->middleware(['jwt.auth', 'permission:manage_client_types']);
// Route::post('client_type', 'ClientTypeController@store')->middleware(['jwt.auth', 'permission:manage_client_types']);
// Route::get('client_type/{id}', 'ClientTypeController@get')->middleware(['jwt.auth', 'permission:manage_client_types']);
// Route::put('client_type/{id}', 'ClientTypeController@update')->middleware(['jwt.auth', 'permission:manage_client_types']);
// Route::delete('client_type/{id}', 'ClientTypeController@delete')->middleware(['jwt.auth', 'permission:manage_client_types']);

// Route::get('drivers', 'DriverController@index')->middleware(['jwt.auth', 'permission:get_drivers']);
// Route::post('driver', 'DriverController@store')->middleware(['jwt.auth', 'permission:manage_drivers']);
// Route::get('driver/{id}', 'DriverController@get')->middleware(['jwt.auth', 'permission:manage_drivers']);
// Route::put('driver/{id}', 'DriverController@update')->middleware(['jwt.auth', 'permission:manage_drivers']);
// Route::delete('driver/{id}', 'DriverController@delete')->middleware(['jwt.auth', 'permission:manage_drivers']);

// Route::get('driver_cars', 'DriverCarController@index')->middleware(['jwt.auth', 'permission:manage_driver_cars']);
// Route::post('driver_car', 'DriverCarController@store')->middleware(['jwt.auth', 'permission:manage_driver_cars']);
// Route::get('driver_car/{id}', 'DriverCarController@get')->middleware(['jwt.auth', 'permission:manage_driver_cars']);
// Route::put('driver_car/{id}', 'DriverCarController@update')->middleware(['jwt.auth', 'permission:manage_driver_cars']);
// Route::delete('driver_car/{id}', 'DriverCarController@delete')->middleware(['jwt.auth', 'permission:manage_driver_cars']);

// // Route::get('driver-tariffs', 'DriverController@index')->middleware(['jwt.auth', 'permission:manage_driver_tariffs']);
// Route::post('driver_tariff', 'DriverTariffController@store')->middleware(['jwt.auth', 'permission:manage_driver_tariffs']);
// Route::get('driver_tariff/{id}', 'DriverTariffController@get')->middleware(['jwt.auth', 'permission:get_driver_tariff']);
// Route::put('driver_tariff/{id}', 'DriverTariffController@update')->middleware(['jwt.auth', 'permission:manage_driver_tariffs']);
// Route::delete('driver_tariff/{id}', 'DriverTariffController@delete')->middleware(['jwt.auth', 'permission:manage_driver_tariffs']);

// Route::get('parkings', 'ParkingController@index')->middleware(['jwt.auth', 'permission:manage_parkings']);
// Route::post('parking', 'ParkingController@store')->middleware(['jwt.auth', 'permission:manage_parkings']);
// Route::get('parking/{id}', 'ParkingController@get')->middleware(['jwt.auth', 'permission:manage_parkings']);
// Route::get('parking-info/{id}', 'ParkingController@getInfo')->middleware(['jwt.auth', 'permission:manage_parkings']);
// Route::post('parking-order', 'ParkingController@order')->middleware(['jwt.auth', 'permission:manage_parkings']);
// Route::put('parking/{id}', 'ParkingController@update')->middleware(['jwt.auth', 'permission:manage_parkings']);
// Route::delete('parking/{id}', 'ParkingController@delete')->middleware(['jwt.auth', 'permission:manage_parkings']);

// Route::get('reports', 'ReportController@index')->middleware(['jwt.auth', 'permission:manage_reports']);
// Route::post('report', 'ReportController@store')->middleware(['jwt.auth', 'permission:manage_reports']);
// Route::get('report/{id}', 'ReportController@get')->middleware(['jwt.auth', 'permission:manage_reports']);
// Route::put('report/{id}', 'ReportController@update')->middleware(['jwt.auth', 'permission:manage_reports']);
// Route::delete('report/{id}', 'ReportController@delete')->middleware(['jwt.auth', 'permission:manage_reports']);

// Route::get('tariffs', 'TariffController@index')->middleware(['jwt.auth', 'permission:manage_tariffs']);
// Route::post('tariff', 'TariffController@store')->middleware(['jwt.auth', 'permission:manage_tariffs']);
// Route::get('tariff/{id}', 'TariffController@get')->middleware(['jwt.auth', 'permission:manage_tariffs']);
// Route::put('tariff/{id}', 'TariffController@update')->middleware(['jwt.auth', 'permission:manage_tariffs']);
// Route::delete('tariff/{id}', 'TariffController@delete')->middleware(['jwt.auth', 'permission:manage_tariffs']);

// Route::get('users', 'UserController@index')->middleware(['jwt.auth', 'permission:manage_users']);
// Route::post('user', 'UserController@store')->middleware(['jwt.auth', 'permission:manage_users']);
// Route::get('user/{id}', 'UserController@get')->middleware(['jwt.auth', 'permission:manage_users']);
// Route::put('user', 'UserController@update')->middleware(['jwt.auth', 'permission:manage_users']);
// Route::put('user/block/{id}', 'UserController@block')->middleware(['jwt.auth', 'permission:manage_users']);
// Route::delete('user/{id}', 'UserController@delete')->middleware(['jwt.auth', 'permission:manage_users']);

// Route::get('user_actions', 'UserActionController@index')->middleware(['jwt.auth', 'permission:manage_user_actions']);
// Route::post('user_action', 'UserActionController@store')->middleware(['jwt.auth', 'permission:manage_user_actions']);
// Route::get('user_action/{id}', 'UserActionController@get')->middleware(['jwt.auth', 'permission:manage_user_actions']);
// Route::put('user_action/{id}', 'UserActionController@update')->middleware(['jwt.auth', 'permission:manage_user_actions']);
// Route::delete('user_action/{id}', 'UserActionController@delete')->middleware(['jwt.auth', 'permission:manage_user_actions']);
