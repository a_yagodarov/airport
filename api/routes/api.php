<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([

],
  function () {

    Route::post('authenticate', 'Auth\LoginController@authenticate');
    Route::get('logout', 'Auth\LoginController@logout')->middleware(['jwt.auth']);;

    Route::get('actions_users', 'ActionsController@index')->middleware(['jwt.auth', 'permission:manage_actions']);
    Route::post('actions_user', 'ActionsController@store')->middleware(['jwt.auth', 'permission:manage_actions']);
    Route::get('actions_user/{id}', 'ActionsController@get')->middleware(['jwt.auth', 'permission:manage_actions']);
    Route::put('actions_user/{id}', 'ActionsController@update')->middleware(['jwt.auth', 'permission:manage_actions']);
    Route::delete('actions_user/{id}', 'ActionsController@delete')->middleware(['jwt.auth', 'permission:manage_actions']);

    Route::get('areas', 'AreaController@index')->middleware(['jwt.auth', 'permission:manage_areas']);
    Route::post('area', 'AreaController@store')->middleware(['jwt.auth', 'permission:manage_areas']);
    Route::get('area/{id}', 'AreaController@get')->middleware(['jwt.auth', 'permission:manage_areas']);
    Route::put('area/{id}', 'AreaController@update')->middleware(['jwt.auth', 'permission:manage_areas']);
    Route::delete('area/{id}', 'AreaController@delete')->middleware(['jwt.auth', 'permission:manage_areas']);

    Route::get('auctions', 'AuctionController@index')->middleware(['jwt.auth', 'permission:manage_auction']);
    Route::post('auction', 'AuctionController@store')->middleware(['jwt.auth', 'permission:manage_auction']);
    Route::post('auction/choose_winner', 'AuctionController@chooseWinner')->middleware(['jwt.auth', 'permission:manage_auction']);
    Route::get('auction/{id}', 'AuctionController@get')->middleware(['jwt.auth', 'permission:manage_auction']);
    Route::put('auction/{id}', 'AuctionController@update')->middleware(['jwt.auth', 'permission:manage_auction']);
    Route::delete('auction/{id}', 'AuctionController@delete')->middleware(['jwt.auth', 'permission:manage_auction']);

    Route::get('brands', 'BrandController@index')->middleware(['jwt.auth', 'permission:manage_brands']);
    Route::post('brand', 'BrandController@store')->middleware(['jwt.auth', 'permission:manage_brands']);
    Route::get('brand/{id}', 'BrandController@get')->middleware(['jwt.auth', 'permission:manage_brands']);
    Route::put('brand/{id}', 'BrandController@update')->middleware(['jwt.auth', 'permission:manage_brands']);
    Route::delete('brand/{id}', 'BrandController@delete')->middleware(['jwt.auth', 'permission:manage_brands']);

// Route::get('cars/order_search', 'CarController@orderSearch');
    Route::get('cars', 'CarController@index')->middleware(['jwt.auth', 'permission:get_cars']);
    Route::get('car_params', 'CarController@getParams')->middleware(['jwt.auth', 'permission:manage_cars']);
    Route::post('car_black_list', 'CarController@blackList')->middleware(['jwt.auth', 'permission:manage_cars']);
    Route::post('car_permission', 'CarController@permission')->middleware(['jwt.auth', 'permission:manage_cars']);
    Route::post('car', 'CarController@store')->middleware(['jwt.auth', 'permission:manage_cars']);
    Route::get('car/{id}', 'CarController@get')->middleware(['jwt.auth', 'permission:manage_cars']);
    Route::put('car/{id}', 'CarController@update')->middleware(['jwt.auth', 'permission:manage_cars']);
    Route::delete('car/{id}', 'CarController@delete')->middleware(['jwt.auth', 'permission:manage_cars']);

    Route::get('car_images', 'CarImageController@index')->middleware(['jwt.auth', 'permission:manage_car_images']);
    Route::post('car_image', 'CarImageController@store')->middleware(['jwt.auth', 'permission:manage_car_images']);
    Route::get('car_image/{id}', 'CarImageController@get')->middleware(['jwt.auth', 'permission:manage_car_images']);
    Route::put('car_image/{id}', 'CarImageController@update')->middleware(['jwt.auth', 'permission:manage_car_images']);
    Route::delete('car_image/{id}', 'CarImageController@delete')->middleware(['jwt.auth', 'permission:manage_car_images']);

    Route::get('car_brands', 'CarBrandController@index')->middleware(['jwt.auth', 'permission:manage_car_brands']);
    Route::post('car_brand', 'CarBrandController@store')->middleware(['jwt.auth', 'permission:manage_car_brands']);
    Route::get('car_brand/{id}', 'CarBrandController@get')->middleware(['jwt.auth', 'permission:manage_car_brands']);
    Route::put('car_brand/{id}', 'CarBrandController@update')->middleware(['jwt.auth', 'permission:manage_car_brands']);
    Route::delete('car_brand/{id}', 'CarBrandController@delete')->middleware(['jwt.auth', 'permission:manage_car_brands']);

    Route::get('client_types', 'ClientTypeController@index')->middleware(['jwt.auth', 'permission:manage_client_types']);
    Route::post('client_type', 'ClientTypeController@store')->middleware(['jwt.auth', 'permission:manage_client_types']);
    Route::get('client_type/{id}', 'ClientTypeController@get')->middleware(['jwt.auth', 'permission:manage_client_types']);
    Route::put('client_type/{id}', 'ClientTypeController@update')->middleware(['jwt.auth', 'permission:manage_client_types']);
    Route::delete('client_type/{id}', 'ClientTypeController@delete')->middleware(['jwt.auth', 'permission:manage_client_types']);

    Route::get('drivers', 'DriverController@index')->middleware(['jwt.auth', 'permission:get_drivers']);
    Route::post('driver', 'DriverController@store')->middleware(['jwt.auth', 'permission:manage_drivers']);
    Route::get('driver/{id}', 'DriverController@get')->middleware(['jwt.auth', 'permission:manage_drivers']);
    Route::put('driver/{id}', 'DriverController@update')->middleware(['jwt.auth', 'permission:manage_drivers']);
    Route::delete('driver/{id}', 'DriverController@delete')->middleware(['jwt.auth', 'permission:manage_drivers']);

    Route::get('driver_cars', 'DriverCarController@index')->middleware(['jwt.auth', 'permission:manage_driver_cars']);
    Route::post('driver_car', 'DriverCarController@store')->middleware(['jwt.auth', 'permission:manage_driver_cars']);
    Route::get('driver_car/{id}', 'DriverCarController@get')->middleware(['jwt.auth', 'permission:manage_driver_cars']);
    Route::put('driver_car/{id}', 'DriverCarController@update')->middleware(['jwt.auth', 'permission:manage_driver_cars']);
    Route::delete('driver_car/{id}', 'DriverCarController@delete')->middleware(['jwt.auth', 'permission:manage_driver_cars']);

// Route::get('driver-tariffs', 'DriverController@index')->middleware(['jwt.auth', 'permission:manage_driver_tariffs']);
    Route::post('driver_tariff', 'DriverTariffController@store')->middleware(['jwt.auth', 'permission:manage_driver_tariffs']);
    Route::get('driver_tariff/{id}', 'DriverTariffController@get')->middleware(['jwt.auth', 'permission:get_driver_tariff']);
    Route::put('driver_tariff/{id}', 'DriverTariffController@update')->middleware(['jwt.auth', 'permission:manage_driver_tariffs']);
    Route::delete('driver_tariff/{id}', 'DriverTariffController@delete')->middleware(['jwt.auth', 'permission:manage_driver_tariffs']);

    Route::get('parkings', 'ParkingController@index')->middleware(['jwt.auth', 'permission:manage_parkings']);
// Route::get('parkings', 'ParkingController@index')->middleware(['jwt.auth', 'permission:manage_parkings']);
    Route::post('parking', 'ParkingController@store')->middleware(['jwt.auth', 'permission:manage_parkings']);
    Route::get('parking/{id}', 'ParkingController@get')->middleware(['jwt.auth', 'permission:manage_parkings']);
    Route::get('parking-info/{id}', 'ParkingController@getInfo')->middleware(['jwt.auth', 'permission:manage_parkings']);
    Route::post('parking-order', 'ParkingController@order')->middleware(['jwt.auth', 'permission:order_from_parking']);
    Route::put('parking/{id}', 'ParkingController@update')->middleware(['jwt.auth', 'permission:manage_parkings']);
    Route::delete('parking/{id}', 'ParkingController@delete')->middleware(['jwt.auth', 'permission:manage_parkings']);

    Route::get('orders', 'OrderController@index')->middleware(['jwt.auth', 'permission:manage_orders']);
    Route::get('orders/most_ordered', 'OrderController@getMostOrdered')->middleware(['jwt.auth', 'permission:manage_orders']);
    Route::get('orders/ordered_by_user', 'OrderController@getOrdersByUser')->middleware(['jwt.auth', 'permission:manage_orders']);
// Route::post('order', 'OrderController@store')->middleware(['jwt.auth', 'permission:manage_orders']);
    Route::post('order_sms', 'OrderController@sendSms')->middleware(['jwt.auth', 'permission:manage_orders']);
    Route::post('order_parking', 'OrderController@addToParking')->middleware(['jwt.auth', 'permission:manage_orders']);
    Route::get('order/{id}', 'OrderController@get')->middleware(['jwt.auth', 'permission:manage_orders']);
// Route::put('order/{id}', 'OrderController@update')->middleware(['jwt.auth', 'permission:manage_orders']);
// Route::delete('order/{id}', 'OrderController@delete')->middleware(['jwt.auth', 'permission:manage_orders']);


    Route::get('participant/{id}', 'AuctionController@getParticipants')->middleware(['jwt.auth', 'permission:manage_reports']);
//    Route::post('participant', 'ReportController@store')->middleware(['jwt.auth', 'permission:manage_reports']);
//    Route::get('participant/{id}', 'ReportController@get')->middleware(['jwt.auth', 'permission:manage_reports']);
//    Route::put('participant/{id}', 'ReportController@update')->middleware(['jwt.auth', 'permission:manage_reports']);
//    Route::delete('participant/{id}', 'ReportController@delete')->middleware(['jwt.auth', 'permission:manage_reports']);

    Route::get('reports', 'ReportController@index')->middleware(['jwt.auth', 'permission:manage_reports']);
    Route::post('report', 'ReportController@store')->middleware(['jwt.auth', 'permission:manage_reports']);
    Route::get('report/{id}', 'ReportController@get')->middleware(['jwt.auth', 'permission:manage_reports']);
    Route::put('report/{id}', 'ReportController@update')->middleware(['jwt.auth', 'permission:manage_reports']);
    Route::delete('report/{id}', 'ReportController@delete')->middleware(['jwt.auth', 'permission:manage_reports']);

    Route::get('requests', 'RequestController@index')->middleware(['jwt.auth', 'permission:manage_requests']);
    Route::get('request/{id}', 'RequestController@get')->middleware(['jwt.auth', 'permission:manage_requests']);
    Route::put('request/{id}', 'RequestController@update')->middleware(['jwt.auth', 'permission:manage_requests']);
    Route::delete('request/{id}', 'RequestController@delete')->middleware(['jwt.auth', 'permission:manage_requests']);

    Route::get('smss', 'SmsController@index')->middleware(['jwt.auth', 'permission:manage_sms']);
    Route::post('sms', 'SmsController@store')->middleware(['jwt.auth', 'permission:manage_sms']);
    Route::get('sms/{id}', 'SmsController@get')->middleware(['jwt.auth', 'permission:manage_sms']);
    Route::put('sms/{id}', 'SmsController@update')->middleware(['jwt.auth', 'permission:manage_sms']);
    Route::delete('sms/{id}', 'SmsController@delete')->middleware(['jwt.auth', 'permission:manage_sms']);

    Route::get('options', 'OptionsController@index');
    Route::post('option', 'OptionsController@store')->middleware(['jwt.auth', 'permission:manage_options']);
    Route::get('option/{id}', 'OptionsController@get')->middleware(['jwt.auth', 'permission:manage_options']);
    Route::put('option/{id}', 'OptionsController@update')->middleware(['jwt.auth', 'permission:manage_options']);
    Route::delete('option/{id}', 'OptionsController@delete')->middleware(['jwt.auth', 'permission:manage_options']);

    Route::get('tariffs', 'TariffController@index')->middleware(['jwt.auth', 'permission:manage_tariffs']);
    Route::post('tariff', 'TariffController@store')->middleware(['jwt.auth', 'permission:manage_tariffs']);
    Route::get('tariff/{id}', 'TariffController@get')->middleware(['jwt.auth', 'permission:manage_tariffs']);
    Route::put('tariff/{id}', 'TariffController@update')->middleware(['jwt.auth', 'permission:manage_tariffs']);
    Route::delete('tariff/{id}', 'TariffController@delete')->middleware(['jwt.auth', 'permission:manage_tariffs']);

    Route::get('users', 'UserController@index')->middleware(['jwt.auth', 'permission:manage_users']);
    Route::post('user', 'UserController@store')->middleware(['jwt.auth', 'permission:manage_users']);
    Route::get('user/{id}', 'UserController@get')->middleware(['jwt.auth', 'permission:manage_users']);
    Route::put('user', 'UserController@update')->middleware(['jwt.auth', 'permission:manage_users']);
    Route::put('user/block/{id}', 'UserController@block')->middleware(['jwt.auth', 'permission:manage_users']);
    Route::delete('user/{id}', 'UserController@delete')->middleware(['jwt.auth', 'permission:manage_users']);

    Route::get('user_action_by_users', 'UserActionController@indexByUser')->middleware(['jwt.auth', 'permission:manage_user_actions']);

    Route::get('user_actions', 'UserActionController@index')->middleware(['jwt.auth', 'permission:manage_user_actions']);
    Route::post('user_action', 'UserActionController@store')->middleware(['jwt.auth', 'permission:manage_user_actions']);
    Route::get('user_action/{id}', 'UserActionController@get')->middleware(['jwt.auth', 'permission:manage_user_actions']);
    Route::put('user_action/{id}', 'UserActionController@update')->middleware(['jwt.auth', 'permission:manage_user_actions']);
    Route::delete('user_action/{id}', 'UserActionController@delete')->middleware(['jwt.auth', 'permission:manage_user_actions']);

  });

Route::get('terminal/parkings', 'ParkingController@index')->middleware(['jwt.auth', 'permission:use_terminal']);
Route::post('terminal/parking-order', 'ParkingController@order')->middleware(['jwt.auth', 'permission:use_terminal']);
Route::get('terminal/cars/order_search', 'CarController@orderSearch')->middleware(['jwt.auth', 'permission:use_terminal']);
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group([
  'prefix' => 'phone',
], function () {
  require base_path('routes/api_phone.php');
});
Route::group([
  'prefix' => 'mifare',
], function () {
  require base_path('routes/api_mifare.php');
});
Route::group([
  'prefix' => 'sms',
], function () {
  require base_path('routes/api_sms.php');
});
