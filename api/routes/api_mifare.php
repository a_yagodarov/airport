<?php

Route::post('authenticate', 'Mifare\Auth\LoginController@authenticate');
Route::get('parking/get-info', 'Mifare\ParkingController@getFullInfo')->middleware(['jwt.auth', 'role:operator|admin']);;
Route::get('parking/add', 'Mifare\ParkingController@addByMifareCardId')->middleware(['jwt.auth', 'role:operator|admin']);;
Route::post('shift/open', 'Mifare\ShiftController@open')->middleware(['jwt.auth', 'role:operator|admin']);;
Route::post('shift/close', 'Mifare\ShiftController@close')->middleware(['jwt.auth', 'role:operator|admin']);;