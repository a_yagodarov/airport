<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCarDriverIdsAuction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auction', function ($table) {
          $table->integer('car_id')->unsigned()->nullable();
          $table->integer('driver_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auction', function ($table) {
          $table->dropColumn('car_id');
          $table->dropColumn('driver_id');
        });
    }
}
