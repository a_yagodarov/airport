<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionsUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('by_user_id')->unsigned()->nullable();
            $table->integer('car_id')->unsigned()->nullable();
            $table->integer('driver_id')->unsigned()->nullable();
            $table->integer('model_id')->unsigned();
            $table->integer('action_id')->unsigned();
            $table->foreign('user_id')
              ->references('id')
              ->on('users')
              ->onDelete('cascade')
              ->change();
            $table->foreign('car_id')
              ->references('id')
              ->on('cars')
              ->onDelete('cascade')
              ->change();
            $table->foreign('driver_id')
              ->references('id')
              ->on('drivers')
              ->onDelete('cascade')
              ->change();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actions');
    }
}
