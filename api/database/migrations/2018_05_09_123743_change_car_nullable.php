<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCarNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->integer('car_brand_id')->unsigned()->nullable()->change();
            $table->integer('class_id')->unsigned()->nullable()->change();
            $table->integer('group_tariff_id')->unsigned()->nullable()->change();
            $table->integer('client_type_id')->unsigned()->nullable()->change();
            $table->integer('brand_id')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('cars', function (Blueprint $table) {
            $table->integer('car_brand_id')->unsigned()->change();
            $table->integer('class_id')->unsigned()->change();
            $table->integer('group_tariff_id')->unsigned()->change();
            $table->integer('client_type_id')->unsigned()->change();
            $table->integer('brand_id')->unsigned()->change();
        });
    }
}
