<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('towns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 64)->default('');
        });

        Schema::create('drivers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 64)->default('')->nullable();
            $table->string('surname', 64)->default('')->nullable();
            $table->string('patronymic', 64)->default('')->nullable();
            $table->integer('town_id')->unsigned()->nullable();
            $table->string('serial_number', 64)->default('')->nullable();
            $table->integer('experience')->nullable();
            $table->string('phone_number')->default('')->nullable();
            $table->string('phone_number_2')->default('')->nullable();
            $table->string('email')->default('')->nullable();
            $table->text('comment')->nullable();
            $table->integer('raiting')->nullable()->default(5);
            $table->integer('license')->nullable();
            $table->string('photo_image', 64)->default('')->nullable();
            $table->string('passport_number', 64)->default('')->nullable();
            $table->string('driver_card_serial_number', 64)->default('')->nullable();
            $table->text('reviews_text')->default('')->nullable();
            $table->text('mvd_review_data')->default('')->nullable();
            $table->foreign('town_id')
                ->references('id')
                ->on('towns')
                ->onDelete('cascade');
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('towns');
        Schema::dropIfExists('drivers');
    }
}
