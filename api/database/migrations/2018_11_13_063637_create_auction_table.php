<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuctionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auction', function (Blueprint $table) {
            $table->increments('id');
            $table->string('address');
            $table->integer('status');
            $table->integer('number_of_passengers')->nullable();
            $table->integer('baby_chair')->nullable();
            $table->integer('baggage_count')->nullable();
            $table->integer('permission_to_drive')->nullable();
            $table->integer('external_baggage')->nullable();
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auction');
    }
}
