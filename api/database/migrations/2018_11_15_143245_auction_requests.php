<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AuctionRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (!Schema::hasTable('auction_requests'))
      {
        Schema::create('auction_requests', function(Blueprint $table) {
          $table->increments('id');
          $table->integer('user_id');
          $table->integer('car_id');
          $table->integer('km_price');
          $table->integer('total_price');
          $table->integer('auction_id')->unsigned();
          $table->foreign('auction_id')->references('id')->on('auction')->onDelete('cascade');
        });
      }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('auction_requests');
    }
}
