<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAuctionColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auction', function(Blueprint $table) {
          $table->integer('client_phone_number')->nullable();
          $table->integer('bank_card_payment')->nullable();
          $table->integer('report_documents')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('auction', function(Blueprint $table) {
        $table->dropColumn('client_phone_number');
        $table->dropColumn('bank_card_payment');
        $table->dropColumn('report_documents');
      });
    }
}
