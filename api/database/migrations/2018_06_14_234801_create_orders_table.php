<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('driver_id')->unsigned();
            $table->integer('car_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('town')->nullable();
            $table->integer('town_center')->nullable();
            $table->integer('km_price')->nullable();
            $table->integer('periphery')->nullable();
            $table->integer('use_new_tariffs')->nullable();
            $table->integer('use_custom_tariff')->nullable();
            $table->string('custom_tariff')->nullable();
            $table->string('type_tariff_use')->nullable();
            $table->string('starting_rate')->nullable();
            $table->string('town_km_price')->nullable();
            $table->string('countryside_km_price')->nullable();
            $table->foreign('driver_id')
                ->references('id')
                ->on('drivers')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('car_id')
                ->references('id')
                ->on('cars')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
