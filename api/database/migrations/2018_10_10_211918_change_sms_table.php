<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('sms', function (Blueprint $table) {
        $table->integer('status')->nullable();
        $table->string('message')->nullable();
        $table->string('send_to')->nullable();
        $table->integer('driver_id')->unsigned();
        $table->foreign('driver_id')
          ->references('id')
          ->on('drivers')
          ->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS=0');
      Schema::table('sms', function (Blueprint $table) {
        $table->dropColumn('status');
        $table->dropColumn('message');
        $table->dropColumn('send_to');
        $table->dropForeign('sms_driver_id_foreign');
        $table->dropColumn('driver_id');
      });
      DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
