<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(RPSeeder::class);
         $this->call(AdminSeeder::class);
         $this->call(CarClassSeeder::class);
//         $this->call(ActionsSeeder::class);
         $this->call(ZonesSeeder::class);
    }
}
