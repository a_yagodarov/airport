<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\Http\Models\User\User();
        $user->username = 'admin';
        $user->first_name = 'Имя';
        $user->surname = 'Фамилия';
        $user->patronymic = 'Отчество';
        $user->password = bcrypt('admin');
        $user->email = 'admin@admin.ru';
        $user->save();
        $user->assignRole('admin');
    }
}
