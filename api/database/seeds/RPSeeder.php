<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 09.12.2017
 * Time: 22:10
 */
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RPSeeder extends Seeder
{
    public function run(){
        app()['cache']->forget('spatie.permission.cache');

        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        DB::table('roles')->truncate();
        DB::table('permissions')->truncate();
        DB::table('role_has_permissions')->truncate();

        $admin = Role::create(['name' => 'admin', 'title' => 'Администратор']);
        $operator = Role::create(['name' => 'operator', 'title' => 'Оператор']);
        $manager = Role::create(['name' => 'manager', 'title' => 'Менеджер']);
        $terminal_user = Role::create(['name' => 'terminal_user', 'title' => 'Пользователь терминала']);
        $driver = Role::create(['name' => 'driver', 'title' => 'Водитель']);

        $use_terminal = Permission::create(['name' => 'use_terminal']);
        $use_terminal->assignRole($terminal_user);

        $get_requests = Permission::create(['name' => 'get_requests']);
        $manage_auction = Permission::create(['name' => 'manage_auction']);
        $manage_requests = Permission::create(['name' => 'manage_requests']);
        $manage_sms = Permission::create(['name' => 'manage_sms']);
        $manage_areas = Permission::create(['name' => 'manage_areas']);
        $manage_orders = Permission::create(['name' => 'manage_orders']);
        $manage_users = Permission::create(['name' => 'manage_users']);
        $manage_actions = Permission::create(['name' => 'manage_actions']);
        $manage_brands = Permission::create(['name' => 'manage_brands']);
        $manage_cars = Permission::create(['name' => 'manage_cars']);
        $get_cars = Permission::create(['name' => 'get_cars']);
        $get_drivers = Permission::create(['name' => 'get_drivers']);
        $get_driver_tariff = Permission::create(['name' => 'get_driver_tariff']);
        $manage_car_images = Permission::create(['name' => 'manage_car_images']);
        $manage_car_brands = Permission::create(['name' => 'manage_car_brands']);
        $manage_client_types = Permission::create(['name' => 'manage_client_types']);
        $manage_drivers = Permission::create(['name' => 'manage_drivers']);
        $manage_driver_cars = Permission::create(['name' => 'manage_driver_cars']);
        $manage_driver_tariffs = Permission::create(['name' => 'manage_driver_tariffs']);
        $manage_parkings = Permission::create(['name' => 'manage_parkings']);
        $manage_reports = Permission::create(['name' => 'manage_reports']);
        $manage_options = Permission::create(['name' => 'manage_options']);
        $manage_tariffs = Permission::create(['name' => 'manage_tariffs']);
        $manage_user_actions = Permission::create(['name' => 'manage_user_actions']);
        $order_from_parking = Permission::create(['name' => 'order_from_parking']);
        $bid_in_auction = Permission::create(['name' => 'bid_in_auction']);

        $manage_auction->assignRole($admin);
        $manage_requests->assignRole($admin);
        $manage_areas->assignRole($admin);
        $manage_sms->assignRole($admin);
        $manage_users->assignRole($admin, $driver);
        $manage_actions->assignRole($admin);
        $manage_brands->assignRole($admin);
        $manage_cars->assignRole($admin);
        $manage_car_images->assignRole($admin);
        $manage_car_brands->assignRole($admin);
        $manage_client_types->assignRole($admin);
        $manage_drivers->assignRole($admin);
        $manage_driver_cars->assignRole($admin);
        $manage_driver_tariffs->assignRole($admin);
        $manage_reports->assignRole($admin);
        $manage_tariffs->assignRole($admin);
        $manage_user_actions->assignRole($admin);
        $manage_options->assignRole($admin);
        $bid_in_auction->assignRole($driver);

        $get_requests->assignRole($admin);
        $manage_orders->assignRole([$admin, $operator, $manager]);
        $order_from_parking->assignRole([$admin, $manager]);
        $get_drivers->assignRole([$admin, $operator, $manager]);
        $get_driver_tariff->assignRole([$admin, $operator, $manager]);
        $get_cars->assignRole([$admin, $operator, $manager]);
        $manage_parkings->assignRole([$admin, $operator, $manager]);

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
