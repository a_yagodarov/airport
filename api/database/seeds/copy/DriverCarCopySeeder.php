<?php

use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 29.06.2018
 * Time: 12:31
 */
class DriverCarCopySeeder extends Seeder
{
    public function run()
    {
        $rows = \Illuminate\Support\Facades\DB::connection('old')->table('driver_car')->select('*')->get();
        foreach ($rows as $row)
        {
            $driverCar = new \App\Http\Models\DriverCar();
            $driverCar->fill([
                'id' => $row->id,
                'driver_id' => $row->driver_id,
                'car_id' => $row->car_id,
                'mifare_card_id' => $row->mifar_card_id,
            ]);
            $driverCar->save();
        }
    }
}