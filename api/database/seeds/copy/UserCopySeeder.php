<?php

use Illuminate\Database\Seeder;

class UserCopySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function getRoleById($id) : string
    {
        return [
            '1' => 'admin',
            '2' => 'operator',
            '3' => 'manager',
        ][$id];
    }
    public function run()
    {
        $rows = \Illuminate\Support\Facades\DB::connection('old')->table('user')->select('*')->get();
        foreach ($rows as $row)
        {
            $user = new \App\Http\Models\User\User();
            $user->id = $row->id;
            $user->username = $row->username;
            $user->email = $row->email;
            $user->password = bcrypt($row->username);
            $user->blocked = $row->blocked_at ? 1 : 0;
            $user->terminal = $row->terminal_id;
            $user->save();
            $user->assignRole($this->getRoleById($row->role_id));
        }
    }
}
