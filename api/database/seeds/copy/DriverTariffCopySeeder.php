<?php

use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 29.06.2018
 * Time: 12:31
 */
class DriverTariffCopySeeder extends Seeder
{
    public function run()
    {
        $rows = \Illuminate\Support\Facades\DB::connection('old')->table('driver_tariff')->select('*')->get();
        $drivers = \App\Http\Models\Driver::all();
        foreach ($rows as $row)
        {
            $groupTariff = new \App\Http\Models\DriverTariff();
            $groupTariff->fill([
                'id' => $row->driver_id,
                'town' => $row->town,
                'town_center' => $row->town_center,
                'km_price' => $row->km_price,
                'periphery' => $row->periferia,
                'use_custom_tariff' => $row->use_single_tariff ? 1 : 0,
                'custom_tariff' => $row->town,
                'type_tariff_use' => 1,
            ]);
            foreach ($drivers as $driver) {
                if ($driver->id == $row->driver_id)
                {
                    $groupTariff->save();
                }
            }
        }
    }
}