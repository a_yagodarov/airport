<?php

use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 29.06.2018
 * Time: 12:31
 */
class GroupTariffCopySeeder extends Seeder
{
    public function run()
    {
        $rows = \Illuminate\Support\Facades\DB::connection('old')->table('group_tariff')->select('*')->get();
        foreach ($rows as $row)
        {
            $groupTariff = new \App\Http\Models\Tariff();
            $groupTariff->fill([
                'id' => $row->id,
                'name' => $row->name,
                'town' => $row->town,
                'town_center' => $row->town_center,
                'km_price' => $row->km_price,
                'periphery' => $row->periferia,
                'comment' => $row->comment,
                'use_custom_tariff' => $row->use_single_tariff,
                'custom_tariff' => '',
                'type_tariff_use' => 1,
            ]);
            $groupTariff->save();
        }
    }
}