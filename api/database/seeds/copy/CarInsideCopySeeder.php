<?php

use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 29.06.2018
 * Time: 12:31
 */
class CarInsideCopySeeder extends Seeder
{
    public function run()
    {
        $rows = \Illuminate\Support\Facades\DB::connection('old')->table('car_inside')->select('*')->get();
        foreach ($rows as $row)
        {
            $cars = \App\Http\Models\Car::all();
            $carInside = new \App\Http\Models\CarImage();
            $carInside->fill([
                'id' => $row->id,
                'car_id' => $row->car_id,
                'image' => $row->image,
            ]);
            foreach ($cars as $car) {
                if ($car->id == $row->car_id)
                {
                    if ($carInside->saveImageFromUrl("http://airportufataxi.ru/web/files/images/".$row->car_id.'/'.$row->image))
                    {
                        $carInside->save();
                        continue;
                    }
                    else
                    {
                        echo "error";
                        break;
                    }
                }
            }
        }
    }
}