import {Component, OnInit, ElementRef, Renderer2} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {
    Parking as Service,
    CarService,
    DriverService,
    DriverTariffService,
    PrintService,
    OptionsService
} from '../../_services/index';
import {UserHelper} from '../../_helpers/user.helper';
import {User as Model} from '../../_models/user';
import {Observable} from 'rxjs/Observable';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {CarImage} from '../../_models/car.image';
import {Driver} from '../../_models/driver';
import {Car} from '../../_models/car';
import Echo from 'laravel-echo';
declare var $: any;
declare var moment: any;
declare var require: any;
declare var window: any;

@Component({
    selector: 'app-parkings',
    templateUrl: './parkings.component.html',
    styleUrls: ['./parkings.component.css'],
    providers: [UserHelper]
})
export class ParkingsComponent implements OnInit {
    pager;
    sort: string = 'id';
    desc: boolean = true;
    options: any;
    search = [];
    errors = [];
    model: any = {};
    carImages = [];
    carClass: any = {};
    carBrand: any = {};
    parking: any = {};
    printModel: any;
    car = new Car();
    driver = new Driver();
    entryModel: any = new FormGroup({
        car_id: new FormControl(''),
        driver_id: new FormControl(''),

        use_new_tariffs: new FormControl(false),

        type_tariff_use: new FormControl(''),

        town: new FormControl(''),
        town_center: new FormControl(''),
        km_price: new FormControl(''),
        periphery: new FormControl(''),

        custom_tariff: new FormControl(''),
        use_custom_tariff: new FormControl(0),

        starting_rate: new FormControl(''),
        town_km_price: new FormControl(''),
        countryside_km_price: new FormControl(''),
    });
    exitModel: any = new FormGroup({
        car_exit_id: new FormControl('')
    });
    cars = [];
    carsExit = [];
    drivers = [];
    roles: any = {};
    columns = [
        {name: 'id', descr: '#'},
        {name: 'driver_id', descr: 'Название'},
    ];
    zone: any;
    models: any;
    count: any;
    page: any = 1;

    constructor(
        private route: ActivatedRoute,
        private service: Service,
        private printService: PrintService,
        private carService: CarService,
        private driverService: DriverService,
        private optionsService: OptionsService,
        private driverTariffService: DriverTariffService,
        private user: UserHelper
    ) {
      window.Pusher = require('pusher-js');
      window.Echo = new Echo({
        broadcaster: 'pusher',
        key: 'BE0PpyquiTXNdrAvQD1Nt0jyQ3CUpNK6Ddr55GALXb8'
      });
      window.Echo.private(`carDroveIn`)
        .listen('CarDroveIn', (e) => {
          console.log(e);
        });
        this.entryModel.get('car_id').valueChanges.subscribe((value) => {
            this.getDrivers(value);
        });
        this.entryModel.get('driver_id').valueChanges.subscribe((value) => {
            console.log(value);
            if (value)
                this.getTariffs(value);
            else {

            }
        });
    }

    entrySubmit() {
        console.log(this.entryModel);
        console.log('entryModelUsbmit');
        this.errors = [];
        this.service.create(this.entryModel.value).subscribe(
            success => {
                this.loadModels();
                $('#entry').modal('hide');
            },
            error => {
                this.errors = error['error'];
            }
        );
    }

    openModal(row) {
        this.setNullValues();
        this.service.getInfo(row['id']).subscribe((data: any) => {
            this.parking = data;
            this.car = Object.assign(this.car, data['car']);
            this.driver = Object.assign(this.driver, data['driver']);
            this.carClass = Object.assign(this.carClass, data['car']['class']);
            data['car']['images'].forEach((value) => {
                var newModel = Object.assign(new CarImage(), value);
                this.carImages.push(newModel);
            });
            this.carBrand = Object.assign(this.carBrand, data['car']['car_brand']);
            $('#profile').modal();
        }, (error) => {
            this.loadModels();
        });
    }

    setNullValues() {
        this.car = new Car;
        this.driver = new Driver;
        this.carClass = {};
        this.carImages = [];
        this.carBrand = {};
    }

    order(parking, zone) {
        if (confirm('Вы уверены что хотите сделать заказ?')) {
            this.options = this.optionsService.getAll().subscribe((data) => {
                this.options = data;
                this.zone = zone;
                const order = Object.assign({}, parking);
                order.zone = zone;
                order.order_from = 'parking';
                this.service.order(order).subscribe(
                    (data) => {
                        this.printModel = data;
                        this.printModel['date'] = this.dateFormat('D/MM/YYYY');
                        this.printModel['time'] = this.dateFormat('HH:mm');
                        this.printModel['zone'] = zone;
                        this.printModel['options'] = this.options;
                        this.loadModels();
                        $('#profile').modal('hide');
                        this.printService.print(this.printModel).subscribe((result) => {
                            console.log(result);
                        }, (error) => {
                            console.log(error);
                        });

                    },
                    error => {
                        this.loadModels();
                        this.closeAllModals();
                    });
            }, error => {
                console.log(error);
            });
        }
    }

    getTariffs(driver_id) {
        if (this.canInputTariff(this.entryModel.controls['car_id'].value)) {
            this.driverTariffService.getById(driver_id).subscribe((data: any) => {
                console.log(this.entryModel);
                let cloneObj = Object.assign({}, this.entryModel.getRawValue(), data);
                this.entryModel.setValue(cloneObj, {
                    emitEvent: false
                });
            });
        }
    }

    getDrivers(car_id) {
        this.driverService.getAll({params: {all: true, car_id: car_id}}).subscribe((data: any) => {
            this.entryModel.controls['driver_id'].setValue('');
            this.drivers = data['models'];
        });
    }

    canInputTariff(car_id) {
        let item: any;
        if (item = this.cars.find(i => i.id == car_id)) {
            if (item['use_group_tariff'] == '1') {
                return false;
            }
            else
                return true;
        }
        return false;
    }

    exitModal() {
        this.carService.getAll({params: {parking: 1, all: true}}).subscribe((data: any) => {
            this.exitModel.reset();
            this.carsExit = data['models'];
            setTimeout(() => {
                $('#car_exit_id').val(null).trigger('change');
                $('#exit').modal();
            }, 50);
        });

    }

    exitSubmit() {
        if (this.exitModel.controls.car_exit_id) {
            this.service.delete(this.exitModel.controls.car_exit_id.value).subscribe(
                success => {
                    this.loadModels();
                    $('#exit').modal('hide');
                },
                error => {
                    this.errors = error;
                }
            );
        }
    }

    entryModal() {
        this.errors = [];
        this.carService.getAll({params: {all: true, parking: 0}}).subscribe((data: any) => {
            this.entryModel.reset();
            this.cars = data['models'];
            setTimeout(() => {
                $('#car_id_select').val(null).trigger('change');
                $('#entry').modal();
            }, 50);
        });

    }

    ngOnInit() {
        $('body').addClass('parking-wrapper');
        this.route.data
            .subscribe(data => {
                console.log(data);
                this.models = data['data']['models'];
                this.count = data['data']['count'];
                this.page = 1;
            });
        this.initSelect2();
    }

    initSelect2() {
        $('#car_id_select').select2({
            allowClear: true,
            placeholder: 'Выберите',
        });
        $('#car_id_select').on('select2:select', (e) => {
            var val = e.target.value;
            this.entryModel.controls['car_id'].setValue(val);
        });
        $('#car_exit_id').select2({
            allowClear: true,
            placeholder: 'Выберите',
        });
        $('#car_exit_id').on('select2:select', (e) => {
            var val = e.target.value;
            this.exitModel.controls['car_exit_id'].setValue(val);
        });
    }

    ngOnDestroy() {
        $('body').removeClass('parking-wrapper');
        this.closeAllModals();
    }

    modalReviewsText() {
        $('#modal_ask').modal();
    }

    closeAllModals() {
        $('#exit').modal('hide');
        $('#entry').modal('hide');
        $('#profile').modal('hide');
        $('#modal_ask').modal('hide');
    }

    getCircles(row: any) {
        var content = '';
        if (row.car.bank_card_payment) content += '<span class="span_point" style="color: green;">&bull;</span>';
        if (row.car.baby_chair) content += '<span class="span_point" style="color: blue;">&bull;</span>';
        if (row.car.external_baggage) content += '<span class="span_point" style="color: yellow;">&bull;</span>';
        if (row.car.report_documents) content += '<span class="span_point" style="color: red;">&bull;</span>';
        return content;
    }

    delete(id: number) {
        if (confirm('Точно удалить?'))
            this.service.delete(id).subscribe(() => {
                this.loadModels();
            });
    }

    setPage(page) {
        this.page = page;
        this.loadModels();
    }

    setSort(data) {
        if (this.sort == data)
            this.desc = !this.desc;
        else
            this.desc = true;
        this.sort = data;
        this.loadModels();
    }


    loadModels() {
        this.service.getAll(this.getParams()).subscribe(data => {
            this.models = data['models'];
            this.count = data['count'];
        });
    }

    inputSearch() {
        this.page = 1;
        this.loadModels();
    }

    getParams() {
        const params = {
            page: String(this.page - 1),
            orderBy: this.sort,
            desc: this.desc,
            all: true,
        };
        const search = this.search;
        for (const key in search) {
            const value = search[key];
            console.log(key + ' ' + value);
            params[key] = value;
        }
        return {params: params};
    }

    logoutOrDefaultPage() {
        this.user.exitFromParking();
    }

    hasError(field) {
        const classList = {
            'has-error': this.errors[field]
        };
        return classList;
    }

    getStars(num) {
        num > 5 ? num = 5 : (num < 0 ? num = 0 : null);
        if (num) {
            let result = '★&nbsp;'.repeat(num - 1);
            result += '★';
            return result;
        }
        return null;
    }

    getWhiteStars(num) {
        if (num <= 5) {
            return '★&nbsp;'.repeat(5 - num);
        } else {
            return '★&nbsp;'.repeat(5);
        }
    }

    getYesOrNo(condition) {
        if (condition) {
            return 'Да';
        } else {
            return 'Нет';
        }
    }

    dateFormat(format) {
        return moment().format(format);
    }
}
