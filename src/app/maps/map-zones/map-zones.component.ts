import {Component, OnInit} from '@angular/core';
import {AreaService} from '../../_services/index';
import {Router, ActivatedRoute} from '@angular/router';

declare var $: any;
declare var google: any;

@Component({
  selector: 'app-map-zones',
  templateUrl: './map-zones.component.html',
  styleUrls: ['./map-zones.component.css']
})
export class MapZonesComponent implements OnInit {
  map: any;
  drawingManager: any;
  zoneCoords: any = [];
  editableOptions = {fillColor: 'purple', fillOpacity: 0.5};
  disabledOptions = {fillColor: 'black', fillOpacity: null};
  result: any = {};
  type: any;
  zones: any = ['town', 'town_center', 'periphery', 'km_price'];

  constructor(private route: ActivatedRoute, private service: AreaService) {
  }

  ngOnInit() {
    this.initGoogleMap();
    this.initPolygons();
  }

  initPolygons() {
    this.route.data.subscribe((data) => {
      var areas = data.data['models'];
      this.zones.forEach((zoneName) => {
        if (areas.find((obj) => {
          return obj['zone'] == zoneName;
        })) {
          this.parseCoordsNadCreatePolygonFromResponse(areas, zoneName);
        }
      });
    });
  }

  parseCoordsNadCreatePolygonFromResponse(areas, zoneName) {
    var coords = [];
    areas.forEach((obj) => {
      if (obj['zone'] == zoneName) {
        coords.push({lat: Number(obj['lat']), lng: Number(obj['lng'])});
      }
    });
    this.createPolygon(coords, zoneName);
  }

  createPolygon(coords, zoneName) {
    var bermudaTriangle = new google.maps.Polygon({
      paths: coords,
      strokeWeight: 5,
      fillColor: 'black',
      fillOpacity: null,
      clickable: true,
      geodesic: true,
      zIndex: 1
    });
    bermudaTriangle.setMap(this.map);
    this.zoneCoords[zoneName] = bermudaTriangle;
  }

  initGoogleMap() {
    this.map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: 54.55, lng: 55.88},
      zoom: 8
    });

    this.drawingManager = new google.maps.drawing.DrawingManager({
      drawingMode: null,
      drawingControl: false,
      markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
      polygonOptions: {
        strokeWeight: 5,
        fillColor: 'purple',
        fillOpacity: 1,
        clickable: true,
        draggable: true,
        geodesic: true,
        editable: true,
        zIndex: 1
      }
    });

    this.drawingManager.setMap(this.map);
  }

  editArea(type) {
    this.type = type;
    if (this.zoneCoords[type] == undefined) {
      this.drawingManager.setDrawingMode('polygon');
      google.maps.event.addListener(this.drawingManager, 'polygoncomplete', (polygon) => {
        if (polygon.fillColor == 'purple') {
          console.log(polygon.editable);
          this.drawingManager.setDrawingMode(null);
          this.zoneCoords[this.type] = polygon;
          this.setDisabled(this.type);
        }
      });
    }
    else {
      this.setEditable(type);
    }
    console.log(this.zoneCoords);
  }

  setDisabled(zone) {
    this.zoneCoords[zone].setEditable(false);
    this.zoneCoords[zone].setOptions(this.disabledOptions);
  }

  setEditable(zone) {
    this.zones.forEach((zoneName) => {
      if (this.zoneCoords[zoneName] != undefined) {
        this.zoneCoords[zoneName].setEditable(false);
        this.zoneCoords[zoneName].setOptions(this.disabledOptions);
      }
    });
    this.zoneCoords[zone].setEditable(true);
    this.zoneCoords[zone].setOptions(this.editableOptions);
  }

  isEditable(zone) {
    let classList = {
      'btn-success': this.zoneCoords[zone].editable,
      'btn-default': !this.zoneCoords[zone].editable
    };
    return classList;
  }

  saveAreas() {
    this.zones.forEach((name) => {
      this.result[name] = [];
      if (this.zoneCoords[name] != undefined) {
        this.zoneCoords[name].getPaths()['j'][0]['j'].forEach(
          (item) => {
            this.result[name].push({lat: item.lat(), lng: item.lng()});
          });
      }
    });
    console.log(this.result);
    this.service.create({areas: this.result}).subscribe((data) => {
      alert('Сохранено');
    }, (data) => {
      // alert('error');
    });
  }

}
