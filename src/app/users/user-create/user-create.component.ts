import { Component, OnInit } from '@angular/core';
import { UserService } from "../../_services/user.service";
import { Router } from '@angular/router';
import { User } from '../../_models/user';
@Component({
  moduleId: module.id.toString(),
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {

    model : any = {};
    user = new User();
    roles : any = {};
    objectKeys = Object.keys;
    errors : any = [];
    constructor(private userService: UserService,private router: Router) {
      this.roles = this.user.getRoles();
    }

    submitted = false;

    onSubmit() { 
      this.userService.create(this.model).subscribe(data => {
        this.router.navigate(['users']);
      },
      error => {
        console.log(error['error']);
        this.errors = error['error'];
      });
      console.log(this.model);
    }

    ngOnInit() {

    }

    hasError(field)
    {
        let classList = {
            'has-error' : this.errors[field]
        };
        return classList;
    }
}
