import {Component, OnInit} from '@angular/core';
import {UserService} from '../../_services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../../_models/user';

@Component({
    moduleId: module.id.toString(),
    templateUrl: './user-edit.component.html',
    styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {

    model: any = {};
    user = new User();
    errors: any = [];
    roles: any = {};
    objectKeys = Object.keys;

    constructor(private userService: UserService, private route: ActivatedRoute, private router: Router) {
        this.roles = this.user.getRoles();
        route.data
            .subscribe(data => {
                    console.log(data);
                    this.model = data.data;
                },
                error => {
                    console.log(error['error']);
                    this.errors = error['error'];
                });
    }

    submitted = false;

    onSubmit() {
        this.userService.update(this.model).subscribe(data => {
                this.router.navigate(['users']);
            },
            error => {
                console.log(error['error']);
                this.errors = error['error'];
            });
        console.log(this.model);
    }

    ngOnInit() {

    }

    hasError(field) {
        let classList = {
            'has-error': this.errors[field]
        };
        return classList;
    }

}
