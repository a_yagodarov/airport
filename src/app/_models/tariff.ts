export class Tariff {
	town : any;
	town_center : any;
	type_tariff_use : any;
	km_price : any;
	periphery : any;
	countryside_km_price : any;
	town_km_price : any;
	use_custom_tariff : any;
	custom_tariff : any;
	tariff_names = {
		'town' : 'Город',
		'town_center' : 'Центр',
		'km_price' : 'Межгород км/руб.',
		'periphery' : 'Периферия',
		'custom_tariff' : 'Тариф',
		'countryside_km_price' : 'Периферия(цена за км.)',
		'town_km_price' : 'Город(цена за км.)',
	};
	zone_tariffs = ['town', 'town_center', 'km_price', 'periphery'];
	km_price_tariffs = ['countryside_km_price', 'town_km_price'];
	getTariffsArray()
	{
		var result = [];
		if (this.type_tariff_use == 1) {
			if (this.use_custom_tariff) {
				result.push({
					'custom_tariff' : this.custom_tariff,
					'text' : this.tariff_names['custom_tariff']
				});
			}
			else
			{
				this.zone_tariffs.forEach((item) => {
					this[item] ? result.push({
						item : this[item],
						text : this.tariff_names[item]
					}) : '';
				});
			}
		}
		else
		{
			this.km_price_tariffs.forEach((item) => {
				this[item] ? result.push({
					item : this[item],
					text : this.tariff_names[item]
				}) : '';
			});	
		}
		return result;
	}
}