export class Driver {
  id: any;
  image: any;
  terminal_countryside_order_priority = 1;
  entry_to_parking = 1;
  raiting = 5;

  hasImage() {
    return this.image;
  }

  getImagePath() {
    if (this.image) {
      return 'api/public/images/drivers/' + this.id + '/' + this.image;
    } else {
      return '';
    }
  }
}
