export class CarImage {
	image : any;
	getImagePath()
	{
		if (this.image){
			return  'api/public/images/car_insides/'+this.image;
		} 
		else
		{
			return  'api/public/images/car_insides/';
		}
	};
}