import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxiOrderedComponent } from './taxi-ordered.component';

describe('TaxiOrderedComponent', () => {
  let component: TaxiOrderedComponent;
  let fixture: ComponentFixture<TaxiOrderedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxiOrderedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxiOrderedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
