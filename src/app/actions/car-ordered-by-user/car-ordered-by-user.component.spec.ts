import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarOrderedByUserComponent } from './car-ordered-by-user.component';

describe('CarOrderedByUserComponent', () => {
  let component: CarOrderedByUserComponent;
  let fixture: ComponentFixture<CarOrderedByUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarOrderedByUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarOrderedByUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
