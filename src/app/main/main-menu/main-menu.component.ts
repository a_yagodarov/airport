import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute,} from '@angular/router';
import {UserHelper} from '../../_helpers/user.helper';
import {Location} from '@angular/common';

declare var $: any;

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.css'],
  providers: [UserHelper]
})
export class MainMenuComponent implements OnInit {
  title = 'app';
  logged: boolean;

  constructor(
    public user: UserHelper,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location
  ) {
    console.log(route);
    this.logged = user.getUserId() ? true : false;
  }

  isActiveUrl(path) {
    if (this.location.path()[2] !== undefined ){
      if (path === this.location.path().split('/')[1] + '/' + this.location.path().split('/')[2])
        return true;
    }
    if (path === this.location.path().split('/')[1])
      return true;
    return false;
  }

  getUrlClass(path) {
    return {
      'active': this.isActiveUrl(path)
    };
  }

  ngOnInit() {
  }

  razvernutIli() {
    $('#razvernut').toggle();
  }
  razvernutMob() {
    $('#razvernutMob').toggle();
  }
  razvernutSprav() {
    $('#razvernutSprav').toggle();
  }
}
