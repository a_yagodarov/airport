import { Component, OnInit } from '@angular/core';
import { TariffService as Service} from "../../_services/index";
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-tariff-edit',
  templateUrl: './tariff-edit.component.html',
  styleUrls: ['./tariff-edit.component.css']
})
export class TariffEditComponent implements OnInit {

    model : any = {};
    errors : any = [];
    constructor(private service: Service, private route:ActivatedRoute, private router: Router) {
      route.data
      .subscribe(data => {
        console.log(data);
        this.model = data.data;
      },
      error => {
        console.log(error['error']);
        this.errors = error['error'];
      });
    }

    submitted = false;

    onSubmit() { 
      this.service.update(this.model).subscribe(data => {
        this.router.navigate(['tariffs']);
      },
      error => {
        console.log(error['error']);
        this.errors = error['error'];
      });
      console.log(this.model);
    }

    ngOnInit() {

    }

    hasError(field)
    {
        let classList = {
            'has-error' : this.errors[field]
        };
        return classList;
    }
}
