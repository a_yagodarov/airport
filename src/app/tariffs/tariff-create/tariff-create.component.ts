import { Component, OnInit } from '@angular/core';
import { TariffService as Service} from "../../_services/tariff.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-tariff-create',
  templateUrl: './tariff-create.component.html',
  styleUrls: ['./tariff-create.component.css']
})
export class TariffCreateComponent implements OnInit {

    model : any = {
      'use_custom_tariff' : 0,
      'type_tariff_use' : 1
    };
    errors : any = [];
    constructor(private service: Service,private router: Router) { }

    submitted = false;

    onSubmit() { 
      this.service.create(this.model).subscribe(data => {
        this.router.navigate(['tariffs']);
      },
      error => {
        this.errors = error['error'];
      });
    }

    ngOnInit() {

    }

    hasError(field)
    {
        let classList = {
            'has-error' : this.errors[field]
        };
        return classList;
    }

    // disabled(field)
    // {
    //     let classList = {
    //         'has-error' : this.model.use_custom_tariff = 1
    //     };
    //     return classList;
    // }
}
