import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from "@angular/common/http";
import { TariffService as Service } from "../../_services/tariff.service";
import { UserHelper } from '../../_helpers/user.helper';
import { EditButtonRenderer } from "../../_extends/childmessage";
@Component({
  selector: 'app-tariffs',
  templateUrl: './tariffs.component.html',
  styleUrls: ['./tariffs.component.css']
})
export class TariffsComponent implements OnInit {
	pager;
	sort: string = "id";
	desc: boolean = true;
	search = [];
	columns = [
		{ name: 'id' , descr: '#' },
		{ name: 'name' , descr: 'Название' }
	];
	models:any;
	count:any;
	page:any=1;
	constructor(private route:ActivatedRoute, private service: Service) {

	}

	ngOnInit() {
		this.route.data
				.subscribe(data => {
					console.log(data);
					this.models = data['data']['models'];
					this.count = data['data']['count'];
					this.page = 1;
				});
	}

	delete(id: number) {
		if (confirm("Точно удалить?"))
			this.service.delete(id).subscribe(() => { this.loadModels() });
	}

	setPage(page){
		this.page = page;
		this.loadModels();
	}

	setSort(data){
		if (this.sort == data)
			this.desc = !this.desc;
		else
			this.desc = true;
		this.sort = data;
		this.loadModels();
	}

	loadModels() {
		this.service.getAll(this.getParams()).subscribe(data => {
			this.models = data['models'];
			this.count = data['count'];
		});
	}

	inputSearch()
    {
        this.page = 1;
        this.loadModels();
    }

	getParams() {
		var params = {
            page:String(this.page - 1),
            orderBy:this.sort,
            desc:this.desc,
        };
		var search = this.search;
        for (let key in search) {
            let value = search[key];
            console.log(key+' '+value);
            params[key] = value;
        }
		return {params:params};
	}
}
