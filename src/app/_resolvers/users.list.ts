/**
 * Created by Алексей on 09.02.2018.
 */
import {Resolve} from "@angular/router";
import {Observable} from "rxjs";
import {RouterStateSnapshot} from "@angular/router";
import {ActivatedRouteSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {UserService} from "../_services/user.service";

@Injectable()
export class UsersListResolver implements Resolve<any[]>{
  constructor(private userService: UserService) {

  }
  resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<any[]> {
    return this.userService.getAll({params:{all:true}});
  }

}