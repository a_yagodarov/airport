/**
 * Created by Алексей on 09.02.2018.
 */
import {Resolve} from "@angular/router";
import {Observable} from "rxjs";
import {RouterStateSnapshot} from "@angular/router";
import {ActivatedRouteSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {CarBrandService} from "../_services/car.brand.service";

@Injectable()
export class CarBrandsResolver implements Resolve<any[]>{
  constructor(private service: CarBrandService) {

  }
  resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<any[]> {
    return this.service.getAll();
  }

}