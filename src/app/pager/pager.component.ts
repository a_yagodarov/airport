import {Component, Input, Output, EventEmitter,  TemplateRef, ElementRef, ViewChildren, ViewContainerRef } from '@angular/core';
import {OnChanges, SimpleChange } from '@angular/core';
import * as _ from 'underscore';
@Component({
  selector: 'pager',
  templateUrl: './pager.component.html',
  styleUrls: ['./pager.component.css']
})
export class Pager {
	pager:any;
    @Input() total:number;
    @Input() currentPage:number;
    @Input() pageSize:number;
    @Output() _setPage = new EventEmitter<any>();
	constructor() {
		this.pager = this.getPager(this.total, this.currentPage);
	}

	ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
       let changedProp = changes['total'];
       if (changedProp)
       {
          this.pager = this.getPager(changedProp.currentValue, 1);
       }
    }
	getPager(totalItems: number, currentPage: number = 1, pageSize: number = 10) {
		// calculate total pages
		let totalPages = Math.ceil(totalItems / pageSize);

		let startPage: number, endPage: number;
		if (totalPages <= 10) {
		// less than 10 total pages so show all
		startPage = 1;
		endPage = totalPages;
		} else {
		// more than 10 total pages so calculate start and end pages
		if (currentPage <= 6) {
		  startPage = 1;
		  endPage = 10;
		} else if (currentPage + 4 >= totalPages) {
		  startPage = totalPages - 9;
		  endPage = totalPages;
		} else {
		  startPage = currentPage - 5;
		  endPage = currentPage + 4;
		}
		}

		// calculate start and end item indexes
		let startIndex = (currentPage - 1) * pageSize;
		let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

		// create an array of pages to ng-repeat in the pager control
		let pages = _.range(startPage, endPage + 1);

		// return object with all pager properties required by the view
		return {
			totalItems: totalItems,
			currentPage: currentPage,
			pageSize: pageSize,
			totalPages: totalPages,
			startPage: startPage,
			endPage: endPage,
			startIndex: startIndex,
			endIndex: endIndex,
			pages: pages
		};
    }

    setPage(page: number) {
      	this._setPage.emit(page);
		this.currentPage = page;
      	this.pager = this.getPager(this.total, this.currentPage);
		//this.pager.currentPage = page;
    }
}
