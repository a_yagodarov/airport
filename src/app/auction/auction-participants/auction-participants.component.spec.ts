import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuctionParticipantsComponent } from './auction-participants.component';

describe('AuctionParticipantsComponent', () => {
  let component: AuctionParticipantsComponent;
  let fixture: ComponentFixture<AuctionParticipantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuctionParticipantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuctionParticipantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
