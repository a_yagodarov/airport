import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {AuctionService as Service} from '../../_services/index';
import {Brand} from '../../_models/brand';

@Component({
  selector: 'app-auctions',
  templateUrl: './auctions.component.html',
  styleUrls: ['./auctions.component.css']
})
export class AuctionsComponent implements OnInit {
  pager;
  sort: string = 'id';
  desc: boolean = true;
  search = [];
  columns = [
    {name: 'id', descr: '#'},
    {name: 'comment', descr: 'Список участников'},
    {name: 'address', descr: 'Адрес'},
    {name: 'comment', descr: 'Комментарий'},
    // {name: 'driver_id', descr: 'Водитель'},
    {name: 'created_at', descr: 'Дата добавления'},
    {name: 'status', descr: 'Статус'},
  ];
  statusList = [
    {
      id: 0,
      label: 'Отменен'
    }, {
      id: 1,
      label: 'Открыт'
    }, {
      id: 2,
      label: 'Завершен'
    },
  ];
  model = new Brand();
  models: any;
  count: any = 0;
  page: any = 1;

  constructor(private route: ActivatedRoute, private router: Router, private service: Service) {

  }

  ngOnInit() {
    this.loadModels();
  }

  getStatus(row) {
    const result = this.statusList.find(obj => {
      return obj.id === row['status'];
    });
    return result['label'];
  }

  getFormattedDate(date) {
    return moment.utc(date).local().format('DD.MM.YYYY HH:mm');
  }

  delete(id: number) {
    if (confirm('Точно удалить?'))
      this.service.delete(id).subscribe(() => {
        this.loadModels();
      });
  }

  setPage(page) {
    this.page = page;
    this.loadModels();
  }

  setSort(data) {
    if (this.sort == data)
      this.desc = !this.desc;
    else
      this.desc = true;
    this.sort = data;
    this.loadModels();
  }

  loadModels() {
    this.service.getAll(this.getParams()).subscribe(data => {
      this.models = data['models'];
      this.count = data['count'];
    });
  }

  inputSearch() {
    this.page = 1;
    this.loadModels();
  }

  getParams() {
    var params = {
      page: String(this.page - 1),
      orderBy: this.sort,
      desc: this.desc,
    };
    var search = this.search;
    for (let key in search) {
      let value = search[key];
      console.log(key + ' ' + value);
      params[key] = value;
    }
    return {params: params};
  }
}
