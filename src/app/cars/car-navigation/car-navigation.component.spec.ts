import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarNavigationComponent } from './car-navigation.component';

describe('CarNavigationComponent', () => {
  let component: CarNavigationComponent;
  let fixture: ComponentFixture<CarNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
