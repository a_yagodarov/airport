import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarDriverEditComponent } from './car-driver-edit.component';

describe('CarDriverEditComponent', () => {
  let component: CarDriverEditComponent;
  let fixture: ComponentFixture<CarDriverEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarDriverEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarDriverEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
