import { Component, OnInit } from '@angular/core';
import { CarImageService as Service} from "../../../_services/index";
import { Router, ActivatedRoute } from '@angular/router';
import { CarImage } from '../../../_models/car.image';
declare var $:any;
@Component({
  selector: 'app-car-images-create',
  templateUrl: './car-images-create.component.html',
  styleUrls: ['./car-images-create.component.css']
})
export class CarImagesCreateComponent implements OnInit {

    model : any = {};
    errors : any = {};
    drivers : any = [];
    constructor(private service: Service,private router: Router, private route:ActivatedRoute) {
    	this.model.car_id = Number(route.snapshot['_urlSegment']['segments'][1]['path']);
    }

    submitted = false;

    onSubmit() {
      this.service.create(this.model).subscribe(data => {
        this.router.navigate(['/cars/'+this.model.car_id+'/images']);
      },
      error => {
        console.log(error['error']);
        this.errors = error['error'];
      });
      console.log(this.model);
    }

    getImageSource()
    {
        return this.model.getImagePath()+this.model.id+'/'+this.model.image;
    }

    onFileChange(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = (e:any) => {
        this.model.file = reader.result.split(',')[1];
        $('#car_img')
          .attr('src', e.target.result)
          .width(250);
      };
    }
  }

    ngOnInit() {

    }

    hasError(field)
    {
        let classList = {
            'has-error' : this.errors[field]
        };
        return classList;
    }
}
