import {Component, OnInit} from '@angular/core';
import {CarImageService as Service} from '../../../_services/index';
import {Router, ActivatedRoute} from '@angular/router';
import {CarImage} from '../../../_models/car.image';

@Component({
    selector: 'app-car-images-edit',
    templateUrl: './car-images-edit.component.html',
    styleUrls: ['./car-images-edit.component.css']
})
export class CarImagesEditComponent implements OnInit {

    model: any = new CarImage();
    errors: any = {};
    drivers: any = [];
    src: any;
    uploaded: boolean = false;

    constructor(private service: Service, private router: Router, private route: ActivatedRoute) {
        this.model.car_id = Number(route.snapshot['_urlSegment']['segments'][1]['path']);
        route.data.subscribe(
            data => {
                this.model = Object.assign(this.model, data['data']);
            });
    }

    submitted = false;

    onSubmit() {
        this.service.create(this.model).subscribe(data => {
                this.router.navigate(['/cars/' + this.model.car_id + '/images']);
            },
            error => {
                console.log(error['error']);
                this.errors = error['error'];
            });
        console.log(this.model);
    }

    getImageSource() {
        if (!this.uploaded)
            return this.model.getImagePath() + this.model.image;
        else
            return this.src;
    }


    ngOnInit() {

    }

    hasError(field) {
        let classList = {
            'has-error': this.errors[field]
        };
        return classList;
    }

    onFileChange(event) {
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = (e: any) => {
                this.uploaded = true;
                this.src = e.target.result;
                this.model.file = reader.result.split(',')[1];
            };
        }
    }
}
