import { Component, OnInit, ElementRef, Renderer2 } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from "@angular/common/http";
import { CarImageService as Service} from "../../../_services/index";
import { UserHelper } from '../../../_helpers/user.helper';
import { CarImage as Model} from '../../../_models/car.image';

@Component({
  selector: 'app-car-images',
  templateUrl: './car-images.component.html',
  styleUrls: ['./car-images.component.css']
})
export class CarImagesComponent implements OnInit {
	pager;
	sort: string = "id";
	car_id : number;
	desc: boolean = true;
	search = [];
	model = new Model();
	roles : any = {};
	columns = [
		{ name: 'image' , descr: 'Фото' }
	];
	models:any;
	count:any;
	page:any=1;
	constructor(private route:ActivatedRoute, private service: Service) {
		this.car_id = Number(route.snapshot['_urlSegment']['segments'][1]['path']);
	}

	ngOnInit() {
		this.route.data
				.subscribe(data => {
					console.log(data);
					this.models = data['data']['models'];
					this.count = data['data']['count'];
					this.page = 1;
				});
	}

	// toggleBlock(model)
	// {
	// 	let update : any = Object.assign({}, model);
	// 	update.blocked = update.blocked == 1 ? 0 : 1;
	// 	this.service.block(update).subscribe(() => {
	// 		model.blocked = model.blocked == 1 ? 0 : 1;
	// 	});
	// }

	delete(id: number) {
		if (confirm("Точно удалить?"))
			this.service.delete(id).subscribe(() => { this.loadModels() });
	}

	setPage(page){
		this.page = page;
		this.loadModels();
	}

	setSort(data){
		if (this.sort == data)
			this.desc = !this.desc;
		else
			this.desc = true;
		this.sort = data;
		this.loadModels();
	}

	loadModels() {
		this.service.getAll(this.getParams()).subscribe(data => {
			this.models = data['models'];
		});
	}

	inputSearch()
    {
        this.page = 1;
        this.loadModels();
    }

	getParams() {
		var params = {
            page:String(this.page - 1),
            orderBy:this.sort,
            desc:this.desc,
            car_id:this.car_id
        };
		var search = this.search;
        for (let key in search) {
            let value = search[key];
            console.log(key+' '+value);
            params[key] = value;
        }
		return {params:params};
	}
}
