import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarImagesWrapperComponent } from './car-images-wrapper.component';

describe('CarImagesWrapperComponent', () => {
  let component: CarImagesWrapperComponent;
  let fixture: ComponentFixture<CarImagesWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarImagesWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarImagesWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
