import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerminalModalComponent } from './terminal-modal.component';

describe('TerminalModalComponent', () => {
  let component: TerminalModalComponent;
  let fixture: ComponentFixture<TerminalModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerminalModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerminalModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
