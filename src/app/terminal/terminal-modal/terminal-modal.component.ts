import {Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter} from '@angular/core';
import {OptionsService, PrintService, TerminalService} from '../../_services';

declare var $: any;

@Component({
    selector: 'app-terminal-modal',
    templateUrl: './terminal-modal.component.html',
    styleUrls: ['./terminal-modal.component.css']
})
export class TerminalModalComponent implements OnChanges, OnInit {
    @Input() model: any;
    @Input() wtf: any;
    @Output()
    order: EventEmitter<any> = new EventEmitter<any>();
    id: any = '#profile';
    options: any;
    constructor( public terminal: TerminalService,
                 private printService: PrintService,
                 private optionsService: OptionsService) {}

    orderCar(model) {
        this.optionsService.getAll().subscribe((options) => {
            model['date'] =  moment().format('D/MM/YYYY');
            model['time'] = moment().format('HH:mm');
            model['order_from'] = 'terminal';
            model['options'] = options;
            this.order.emit(model);
            this.terminal.order(model).subscribe((data) => {
                $('#terminal-steps').steps('next');
                setTimeout(() => {
                    window.location.reload();
                }, 15000);
            }, (error) => {

            });
            this.printService.print(model).subscribe((data) => {
            });
            this.hideModal();
        });
    }

    ngOnInit() {
        console.log(this.model);
    }

    modalReviewsText() {
        $('#modal_ask').modal();
    }

    ngOnChanges(changes: SimpleChanges) {
        console.log(this.model);
        if (this.model)
            this.showModal();
    }

    getStars(num) {
        num > 5 ? num = 5 : (num < 0 ? num = 0 : null);
        if (num) {
            let result = '★&nbsp;'.repeat(num - 1);
            result += '★';
            return result;
        }
        return null;
    }

    getWhiteStars(num) {
        if (num <= 5) {
            return '★&nbsp;'.repeat(5 - num);
        } else {
            return '★&nbsp;'.repeat(5);
        }
    }

    getYesOrNo(condition) {
        if (condition)
            return 'Да';
        else
            return 'Нет';
    }

    showModal() {
        $(this.id).modal();
    }

    hideModal() {
        $(this.id).modal('hide');
    }
}
