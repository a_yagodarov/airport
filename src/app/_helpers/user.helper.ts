import {Injectable} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {AuthenticationService} from '../_services';

@Injectable()
export class UserHelper {
    constructor(private router: Router, private service: AuthenticationService) {

    }

    public getUserName() {
        var storage = JSON.parse(localStorage.getItem('user'));
        if (storage && storage.user)
            return storage.user.username;
        else
            return false;
    }

    public getUserId() {
        var storage = JSON.parse(localStorage.getItem('user'));
        if (storage && storage.user)
            return storage.user.id;
        else
            return false;
    }

    exitFromParking() {
        switch (this.getUserRole()) {
            case 'admin': {
                this.router.navigate([this.getStartUrl()]);
                break;
            }
            case 'manager' : {
                this.router.navigate([this.getStartUrl()]);
                break;
            }
            case 'operator' :
                this.logout();
        }
    }

    logout() {
        this.service.logout().subscribe((data) => {
          localStorage.removeItem('user');
          this.router.navigate([this.getLoginUrl()]);
        }, (error) => {
          localStorage.removeItem('user');
          this.router.navigate([this.getLoginUrl()]);
        });

    }

    getUserRole() {
        var storage = JSON.parse(localStorage.getItem('user'));
        if (storage && storage.role)
            return storage.role;
        else
            return false;
    }

    getNavBar() {

    }

    navigateAfterLogin() {
        switch (this.getUserRole()) {
            case 'manager' : {
                window.open('/parking', '_blank');
            }
        }
    }

    getStartUrl() {
        return this.getReturnUrl();
    }

    getLoginUrl() {
        return 'login';
    }

    getReturnUrl() {
        switch (this.getUserRole()) {
            case 'admin':
                return 'users';
            case 'manager' :
                return 'orders';
            case 'operator' :
                return 'parking';
            case 'terminal_user' :
                return 'terminal';
        }
    }
}
