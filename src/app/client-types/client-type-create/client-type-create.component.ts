import { Component, OnInit } from '@angular/core';
import { ClientTypeService as Service} from "../../_services/index";
import { Router } from '@angular/router';
@Component({
  selector: 'app-client-type-create',
  templateUrl: './client-type-create.component.html',
  styleUrls: ['./client-type-create.component.css']
})
export class ClientTypeCreateComponent implements OnInit {

    model : any = {};
    errors : any = {};
    constructor(private service: Service,private router: Router) { }

    submitted = false;

    onSubmit() { 
      this.service.create(this.model).subscribe(data => {
        this.router.navigate(['client_types']);
      },
      error => {
        console.log(error['error']);
        this.errors = error['error'];
      });
      console.log(this.model);
    }

    ngOnInit() {

    }

    hasError(field)
    {
        let classList = {
            'has-error' : this.errors[field]
        };
        return classList;
    }
}
