import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhoneRegisterRequestsComponent } from './phone-register-requests.component';

describe('PhoneRegisterRequestsComponent', () => {
  let component: PhoneRegisterRequestsComponent;
  let fixture: ComponentFixture<PhoneRegisterRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhoneRegisterRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhoneRegisterRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
