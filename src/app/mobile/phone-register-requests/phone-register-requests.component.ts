import { Component, OnInit } from '@angular/core';
import {RequestService as Service} from '../../_services';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-phone-register-requests',
  templateUrl: './phone-register-requests.component.html',
  styleUrls: ['./phone-register-requests.component.css']
})
export class PhoneRegisterRequestsComponent implements OnInit {
  pager;
  sort: string = 'id';
  desc: boolean = true;
  search = [];
  columns = [
    {name: '', descr: 'Водитель'},
    {name: 'status', descr: 'Статус'},
  ];
  models: any;
  count: any = 0;
  page: any = 1;

  constructor(private route: ActivatedRoute, private service: Service) {
    this.loadModels();
  }

  ngOnInit() {
  }

  delete(id: number) {
    if (confirm('Точно удалить?'))
      this.service.delete(id).subscribe(() => {
        this.loadModels();
      });
  }

  getDriversName(row)
  {
    return row['driver']['surname'] + ' ' + row['driver']['first_name'];
  }

  setPage(page) {
    this.page = page;
    this.loadModels();
  }

  setSort(data) {
    if (this.sort == data)
      this.desc = !this.desc;
    else
      this.desc = true;
    this.sort = data;
    this.loadModels();
  }

  loadModels() {
    this.service.getAll(this.getParams()).subscribe(data => {
      this.models = data['models'];
      this.count = data['count'];
    });
  }

  inputSearch() {
    this.page = 1;
    this.loadModels();
  }

  toggleBlock(row) {
    let update: any = Object.assign({}, row);
    update.status = row.status === 0 ? 1 : 0;
    console.log(row.status);
    console.log(update.status);
    this.service.update(update).subscribe(() => {
      this.loadModels();
    });
  }

  getParams() {
    var params = {
      page: String(this.page - 1),
      orderBy: this.sort,
      desc: this.desc,
    };
    var search = this.search;
    for (let key in search) {
      let value = search[key];
      console.log(key + ' ' + value);
      params[key] = value;
    }
    return {params: params};
  }
}
