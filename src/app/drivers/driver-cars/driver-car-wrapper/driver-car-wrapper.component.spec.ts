import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverCarWrapperComponent } from './driver-car-wrapper.component';

describe('DriverCarWrapperComponent', () => {
  let component: DriverCarWrapperComponent;
  let fixture: ComponentFixture<DriverCarWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverCarWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverCarWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
