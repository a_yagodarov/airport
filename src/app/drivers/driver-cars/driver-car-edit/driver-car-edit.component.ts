import { Component, OnInit } from '@angular/core';
import { DriverCarService as Service} from "../../../_services/index";
import { ActivatedRoute, Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-driver-car-edit',
  templateUrl: './driver-car-edit.component.html',
  styleUrls: ['./driver-car-edit.component.css']
})
export class DriverCarEditComponent implements OnInit {

  model : any = {};
  driver_id : number;
  errors : any = [];
  cars : any = [];
  constructor(private service: Service, private route:ActivatedRoute, private router: Router) {
    this.driver_id = Number(route.snapshot['_urlSegment']['segments'][1]['path']);
    route.data
        .subscribe(
            data => {
              console.log(data);
              this.cars = data.cars.models;
              this.model = data.data;
            },
            error => {
              this.errors = error['error'];
            });
  }

  submitted = false;
  onSubmit() {
    this.service.update(this.model).subscribe(data => {
          this.router.navigate(['driver/'+this.driver_id+'/cars/']);
        },
        error => {
          console.log(error['error']);
          this.errors = error['error'];
        });
    console.log(this.model);
  }

  ngOnInit() {

  }

  hasError(field)
  {
    let classList = {
      'has-error' : this.errors[field]
    };
    return classList;
  }
}
