import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverCarEditComponent } from './driver-car-edit.component';

describe('DriverCarEditComponent', () => {
  let component: DriverCarEditComponent;
  let fixture: ComponentFixture<DriverCarEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverCarEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverCarEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
