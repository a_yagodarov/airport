import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverCarsComponent } from './driver-cars.component';

describe('DriverCarsComponent', () => {
  let component: DriverCarsComponent;
  let fixture: ComponentFixture<DriverCarsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverCarsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverCarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
