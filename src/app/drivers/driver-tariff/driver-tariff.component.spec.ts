import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverTariffComponent } from './driver-tariff.component';

describe('DriverTariffComponent', () => {
  let component: DriverTariffComponent;
  let fixture: ComponentFixture<DriverTariffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverTariffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverTariffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
