import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-driver-navigation',
  templateUrl: './driver-navigation.component.html',
  styleUrls: ['./driver-navigation.component.css']
})
export class DriverNavigationComponent implements OnInit {
	id : any;
	constructor(private route: ActivatedRoute) {
		this.id = this.route.snapshot.paramMap.get('id');
	}

	ngOnInit() {
		
	}

}
