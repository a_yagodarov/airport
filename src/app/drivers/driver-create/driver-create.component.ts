import {Component, OnInit} from '@angular/core';
import {DriverService as Service} from '../../_services/driver.service';
import {Router} from '@angular/router';
import {Driver} from '../../_models';

declare var $: any;

@Component({
  selector: 'app-driver-create',
  templateUrl: './driver-create.component.html',
  styleUrls: ['./driver-create.component.css']
})
export class DriverCreateComponent implements OnInit {

  model: any = new Driver;
  errors: any = {};

  constructor(private service: Service, private router: Router) {
  }

  submitted = false;

  onSubmit() {
    this.service.create(this.model).subscribe(data => {
        this.router.navigate(['driver/' + data['id'] + '/edit']);
      },
      error => {
        console.log(error['error']);
        this.errors = error['error'];
      });
    console.log(this.model);
  }

  onFileChange(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = (e: any) => {
        this.model.file = reader.result.split(',')[1];
        $('#driver_img')
          .attr('src', e.target.result)
          .width(250);
      };
    }
  }

  ngOnInit() {

  }

  hasError(field) {
    let classList = {
      'has-error': this.errors[field]
    };
    return classList;
  }
}
