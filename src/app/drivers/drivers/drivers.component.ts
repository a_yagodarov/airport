import {Component, OnInit, ElementRef, Renderer2} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {DriverService} from '../../_services/driver.service';
import {UserHelper} from '../../_helpers/user.helper';

declare var $: any;

@Component({
    selector: 'app-drivers',
    templateUrl: './drivers.component.html',
    styleUrls: ['./drivers.component.css'],
    providers: [UserHelper],
})

export class DriversComponent implements OnInit {
    pager;
    sort: string = 'id';
    desc: boolean = true;
    search = [];
    columns = [
        {name: 'id', descr: '#'},
        {name: 'first_name', descr: 'Имя'},
        {name: 'surname', descr: 'Фамилия'},
        {name: 'patronymic', descr: 'Отчество'},
        {name: 'phone_number', descr: 'Телефон'},
        {name: 'experience', descr: 'Стаж вождения'},
    ];
    models: any;
    count: any;
    page: any = 1;

    constructor(private route: ActivatedRoute, private service: DriverService) {

    }

    ngOnInit() {
        this.route.data
            .subscribe(data => {
                console.log(data);
                this.models = data['data']['models'];
                this.count = data['data']['count'];
                this.page = 1;
            });
    }

    clearSearch() {
        this.search = [];
        this.inputSearch();
    }

    delete(id: number) {
        if (confirm('Точно удалить?'))
            this.service.delete(id).subscribe(() => {
                this.loadModels();
            });
    }

    setPage(page) {
        this.page = page;
        this.loadModels();
    }

    setSort(data) {
        if (this.sort == data)
            this.desc = !this.desc;
        else
            this.desc = true;
        this.sort = data;
        this.loadModels();
    }

    loadModels() {
        this.service.getAll(this.getParams()).subscribe(data => {
            this.models = data['models'];
            this.count = data['count'];
        });
    }

    inputSearch() {
        this.page = 1;
        this.loadModels();
    }

    getParams() {
        var params = {
            page: String(this.page - 1),
            orderBy: this.sort,
            desc: this.desc,
        };
        var search = this.search;
        for (let key in search) {
            let value = search[key];
            console.log(key + ' ' + value);
            params[key] = value;
        }
        return {params: params};
    }
}