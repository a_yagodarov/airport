import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ParkingModel, Car, CarImage, Driver,} from '../../_models/index';
import {PrintService, OrderService, OptionsService} from '../../_services/index';

declare var $: any;
declare var moment: any;

@Component({
    selector: 'app-order-view',
    templateUrl: './order-view.component.html',
    styleUrls: ['./order-view.component.css']
})
export class OrderViewComponent implements OnInit {
    model: any = {};
    printModel: any = {};
    user = new ParkingModel();
    errors: any = [];
    roles: any = {};
    objectKeys = Object.keys;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private printService: PrintService,
        private orderService: OrderService,
        private optionsService: OptionsService) {
        route.data
            .subscribe(data => {
                    var item = data['data'];
                    var model = Object.assign(new ParkingModel, item);
                    model.car = Object.assign(new Car, item['car']);
                    model.driver = Object.assign(new Driver, item['driver']);
                    model.car_images = [];
                    if (data.data['car']['images'])
                        item['car']['images'].forEach((item) => {
                            model['car_images'].push(Object.assign(new CarImage, item));
                        });
                    this.model = model;
                },
                error => {
                    console.log(error['error']);
                    this.errors = error['error'];
                });
    }

    submitted = false;

    onSubmit() {

    }

    addToParking(model) {
        this.orderService.addToParking(model).subscribe((result) => {
            this.router.navigate(['/orders']);
        }, (error) => {
            console.log(error);
        });
    }

    sendSms(data, zone) {
        var sendSmsModel = data;
        sendSmsModel['zone'] = zone;
        this.orderService.sendSms(sendSmsModel).subscribe((result) => {
            alert('Успешно');
        }, (error) => {
            console.log(error);
        });
    }

    print(data, zone) {
        this.optionsService.getAll().subscribe((options) => {
            this.printModel = data;
            this.printModel['zone'] = zone;
            this.printModel['options'] = options;
            this.printModel['date'] = this.dateFormat('D/MM/YYYY');
            this.printModel['time'] = this.dateFormat('HH:mm');
            this.printService.print(this.printModel).subscribe((result) => {
                alert('Успешно');
            }, (error) => {
                console.log(error);
            });
        })
    }

    modalReviewsText() {
        $('#modal_ask').modal();
    }

    getStars(num) {
        num > 5 ? num = 5 : (num < 0 ? num = 0 : null);
        var result = '★&nbsp;'.repeat(num - 1);
        result += '★';
        return result;
    }

    getWhiteStars(num) {
        if (num <= 5)
            return '★&nbsp;'.repeat(5 - num);
        else
            return '';
    }

    getYesOrNo(condition) {
        if (condition)
            return 'Да';
        else
            return 'Нет';
    }

    ngOnInit() {

    }

    hasError(field) {
        let classList = {
            'has-error': this.errors[field]
        };
        return classList;
    }

    dateFormat(format) {
        return moment().format(format);
    }
}
