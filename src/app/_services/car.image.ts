import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class CarImageService {
    constructor(private http: HttpClient) { }

    getAll(params= {}) {
        return this.http.get<any[]>('/api/car_images', params);
    }

    getById(id: number) {
        return this.http.get<any>('/api/car_image/' + id);
    }

    create(model: any) {
        return this.http.post('/api/car_image', model);
    }

    update(model: any) {
        return this.http.put('/api/car_image/' + model.id, model);
    }

    delete(id: number) {
        return this.http.delete('/api/car_image/' + id);
    }
}