import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class ActionsUserService {
  name = 'actions_user';

  constructor(private http: HttpClient) {
  }

  getAll(params: any = {}) {
    params.model_id = 1;
    return this.http.get<any[]>('/api/' + this.name + 's', params);
  }

  getById(id: number) {
    return this.http.get<any>('/api/' + this.name + '/' + id);
  }

  create(model: any) {
    return this.http.post('/api/' + this.name, model);
  }

  update(model: any) {
    return this.http.put('/api/' + this.name + '/' + model.id, model);
  }

  delete(id: number) {
    return this.http.delete('/api/' + this.name + '/' + id);
  }
}
