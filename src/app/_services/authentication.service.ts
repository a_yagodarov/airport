﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient) { }

    login(username: string, password: string) {
        return this.http.post<any>('api/authenticate', { username: username, password: password })
            .map(
                token => {
                // login successful if there's a jwt token in the response
                if (token) {
                    console.log(token);
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('user', JSON.stringify(token));
                }

                return token;
            });
    }

    logout() {
        // remove user from local storage to log user out
      return this.http.get<any>('api/logout');
    }
}
