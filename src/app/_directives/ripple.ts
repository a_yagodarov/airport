import { Component, OnInit, Directive, Input, ElementRef } from '@angular/core';
declare var componentHandler: any;
declare var $: any;

@Directive({
  selector: '[ripple]'
})
export class Ripple {
  @Input() ripple;

  constructor(el: ElementRef) {
    // $(".ripple").ripple();
    $(el.nativeElement).ripple();
  }
}