import {AuthGuard} from './_guards';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {UsersComponent} from './users/users/users.component';
import {UserCreateComponent} from './users/user-create/user-create.component';
import {UserEditComponent} from './users/user-edit/user-edit.component';
import {
  UsersResolver, UserEditResolver, DriversResolver, DriverEditResolver, ClientTypesResolver, ClientTypeEditResolver,
  CarBrandsResolver, BrandsResolver, CarBrandEditResolver, BrandEditResolver, TariffsResolver, TariffEditResolver,
  DriverTariffEditResolver, DriverCarsResolver, CarsResolver, CarEditResolver, CarCreateResolver, CarsListResolver, DriverCarEditResolver, CarDriversResolver, DriversListResolver, CarImagesResolver, CarImageEditResolver, ParkingResolver, UsersListResolver, AreasResolver, OrdersResolver, OrderResolver, TerminalResolver, SmsResolver,
  ParticipantsResolver
} from './_resolvers';
import {DriversComponent} from './drivers/drivers/drivers.component';
import {DriverCreateComponent} from './drivers/driver-create/driver-create.component';
import {DriverEditComponent} from './drivers/driver-edit/driver-edit.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {CarBrandsComponent} from './car-brands/car-brands/car-brands.component';
import {BrandsComponent} from './brands/brands/brands.component';
import {CarDriversComponent} from './cars/car-drivers/car-drivers/car-drivers.component';
import {CarBrandsCreateComponent} from './car-brands/car-brands-create/car-brands-create.component';
import {CarBrandsEditComponent} from './car-brands/car-brands-edit/car-brands-edit.component';
import {CarImagesComponent} from './cars/car-images/car-images/car-images.component';
import {BrandsCreateComponent} from './brands/brands-create/brands-create.component';
import {BrandsEditComponent} from './brands/brands-edit/brands-edit.component';
import {TariffsComponent} from './tariffs/tariffs/tariffs.component';
import {TariffCreateComponent} from './tariffs/tariff-create/tariff-create.component';
import {TariffEditComponent} from './tariffs/tariff-edit/tariff-edit.component';
import {ClientTypesComponent} from './client-types/client-types/client-types.component';
import {ClientTypeCreateComponent} from './client-types/client-type-create/client-type-create.component';
import {ClientTypeEditComponent} from './client-types/client-type-edit/client-type-edit.component';
import {DriverNavigationComponent} from './drivers/driver-navigation/driver-navigation.component';
import {DriverTariffComponent} from './drivers/driver-tariff/driver-tariff.component';
import {DriverCarsComponent} from './drivers/driver-cars/driver-cars/driver-cars.component';
import {DriverCarEditComponent} from './drivers/driver-cars/driver-car-edit/driver-car-edit.component';
import {DriverCarWrapperComponent} from './drivers/driver-cars/driver-car-wrapper/driver-car-wrapper.component';
import {DriverCarCreateComponent} from './drivers/driver-cars/driver-car-create/driver-car-create.component';
import {CarsComponent} from './cars/cars/cars.component';
import {CarEditComponent} from './cars/car-edit/car-edit.component';
import {CarCreateComponent} from './cars/car-create/car-create.component';
import {CarDriversWrapperComponent} from './cars/car-drivers/car-drivers-wrapper/car-drivers-wrapper.component';
import {CarDriverCreateComponent} from './cars/car-drivers/car-driver-create/car-driver-create.component';
import {CarDriverEditComponent} from './cars/car-drivers/car-driver-edit/car-driver-edit.component';
import {CarNavigationComponent} from './cars/car-navigation/car-navigation.component';
import {ParkingComponent} from './parking/parking/parking.component';
import {ParkingsComponent} from './parking/parkings/parkings.component';
import {ParkingEntryComponent} from './parking/parking-entry/parking-entry.component';
import {ParkingExitComponent} from './parking/parking-exit/parking-exit.component';
import {CarImagesWrapperComponent} from './cars/car-images/car-images-wrapper/car-images-wrapper.component';
import {CarImagesCreateComponent} from './cars/car-images/car-images-create/car-images-create.component';
import {CarImagesEditComponent} from './cars/car-images/car-images-edit/car-images-edit.component';
import {MainMenuComponent} from './main/main-menu/main-menu.component';
import {MapZonesComponent} from './maps/map-zones/map-zones.component';
import {TerminalComponent} from './terminal/terminal/terminal.component';
import {TerminalPrintComponent} from './terminal/terminal-print/terminal-print.component';
import {OrdersComponent} from './orders/orders/orders.component';
import {OrderViewComponent} from './orders/order-view/order-view.component';
import {OptionsComponent} from './options/options.component';
import {OptionsResolver} from './_resolvers';
import {SmsComponent} from './sms/sms/sms.component';
import {ActionsUserResolver} from './_resolvers/actions_user';
import {ActionsUserComponent} from './actions/actions-user/actions-user.component';
import {TaxiOrderedComponent} from './actions/taxi-ordered/taxi-ordered.component';
import {CarOrderedByUserComponent} from './actions/car-ordered-by-user/car-ordered-by-user.component';
import {PhoneRegisterRequestsComponent} from './mobile/phone-register-requests/phone-register-requests.component';
import {AuctionsComponent} from './auction/auctions/auctions.component';
import {AuctionCreateComponent} from './auction/auction-create/auction-create.component';
import {AuctionParticipantsComponent} from './auction/auction-participants/auction-participants.component';

const appRoutes: any = [
    {
        path: '', component: MainMenuComponent, children: [
        {
          path: 'actions_user', component: ActionsUserComponent, canActivate: [AuthGuard],
          resolve: {
            data: ActionsUserResolver
          }
        },
        {
          path: 'actions/taxi_ordered',
          component: TaxiOrderedComponent
        },
        {
          path: 'mobile/phone_register_requests',
          component: PhoneRegisterRequestsComponent
        },
        {
          path: 'mobile/auctions',
          component: AuctionsComponent
        },
        {
          path: 'mobile/auction-participants/:id',
          component: AuctionParticipantsComponent,
          resolve: {
            data: ParticipantsResolver
          }
        },
        {
          path: 'mobile/auction-create',
          component: AuctionCreateComponent
        },
        {
          path: 'actions/car_ordered_by_user',
          component: CarOrderedByUserComponent
        },
            {path: 'not-found', component: NotFoundComponent},
            {
              path: 'sms', component: SmsComponent, canActivate: [AuthGuard],
              resolve: {
                data: SmsResolver
              }
            },
            {
                path: 'options', component: OptionsComponent, canActivate: [AuthGuard],
                resolve: {
                    data: OptionsResolver
                }
            },
            {
              path: 'users', component: UsersComponent, canActivate: [AuthGuard],
              resolve: {
                data: UsersResolver
              }
            },
            {path: 'users/new', component: UserCreateComponent, canActivate: [AuthGuard]},
            {
                path: 'users/edit/:id', component: UserEditComponent, canActivate: [AuthGuard],
                resolve: {
                    data: UserEditResolver
                }
            },
            {
                path: 'cars', component: CarsComponent, canActivate: [AuthGuard],
                resolve: {
                    data: CarsResolver
                }
            },
            {
                path: 'cars/new', component: CarCreateComponent, canActivate: [AuthGuard],
                resolve: {
                    data: CarCreateResolver
                }
            },
            {
                path: 'cars/:id', component: CarNavigationComponent, canActivate: [AuthGuard],
                children: [
                    {path: '', redirectTo: '/edit', pathMatch: 'full'},
                    {
                        path: 'edit', component: CarEditComponent, canActivate: [AuthGuard],
                        resolve: {
                            data: CarEditResolver,
                            params: CarCreateResolver
                        }
                    },
                    {
                        path: 'drivers', component: CarDriversWrapperComponent, canActivate: [AuthGuard],
                        children: [
                            {
                                path: '', component: CarDriversComponent, canActivate: [AuthGuard],
                                resolve: {
                                    data: CarDriversResolver
                                }
                            },
                            {
                                path: 'new', component: CarDriverCreateComponent, canActivate: [AuthGuard],
                                resolve: {
                                    drivers: DriversListResolver
                                }
                            },
                            {
                                path: 'edit/:id', component: CarDriverEditComponent, canActivate: [AuthGuard],
                                resolve: {
                                    drivers: DriversListResolver,
                                    data: DriverCarEditResolver,
                                }
                            },
                        ],
                    },
                    {
                        path: 'images', component: CarImagesWrapperComponent, canActivate: [AuthGuard],
                        children: [
                            {
                                path: '', component: CarImagesComponent, canActivate: [AuthGuard], resolve: {
                                    data: CarImagesResolver
                                }
                            },
                            {
                                path: 'new', component: CarImagesCreateComponent, canActivate: [AuthGuard]
                            },
                            {
                                path: 'edit/:id', component: CarImagesEditComponent, canActivate: [AuthGuard], resolve: {
                                    data: CarImageEditResolver
                                }
                            }
                        ]
                    }
                ]
            },
            {
                path: 'orders',
                component: OrdersComponent,
                canActivate: [AuthGuard],
                resolve: {
                    data: OrdersResolver
                }
            },
            {
                path: 'order/order_view/:id',
                component: OrderViewComponent,
                canActivate: [AuthGuard],
                resolve: {
                    data: OrderResolver
                }
            },
            {
                path: 'drivers', component: DriversComponent, canActivate: [AuthGuard], resolve: {data: DriversResolver}
            },
            {path: 'drivers/new', component: DriverCreateComponent, canActivate: [AuthGuard]},
            {
                path: 'driver/:id', component: DriverNavigationComponent, canActivate: [AuthGuard],
                children: [
                    {path: '', redirectTo: '/drivers', pathMatch: 'full'},
                    {
                        path: 'edit', component: DriverEditComponent, canActivate: [AuthGuard],
                        resolve: {
                            data: DriverEditResolver
                        }
                    },
                    {
                        path: 'tariff', component: DriverTariffComponent, canActivate: [AuthGuard],
                        resolve: {
                            data: DriverTariffEditResolver
                        }
                    },
                    {
                        path: 'cars', component: DriverCarWrapperComponent, canActivate: [AuthGuard],
                        children: [
                            {
                                path: '', component: DriverCarsComponent, canActivate: [AuthGuard], resolve: {
                                    data: DriverCarsResolver
                                }
                            },
                            {
                                path: 'edit/:id', component: DriverCarEditComponent, canActivate: [AuthGuard], resolve: {
                                    data: DriverCarEditResolver,
                                    cars: CarsListResolver
                                }
                            },
                            {
                                path: 'new', component: DriverCarCreateComponent, canActivate: [AuthGuard], resolve: {
                                    cars: CarsListResolver
                                }
                            }
                        ]
                    }
                ]
            },
            {
                path: 'brands', component: BrandsComponent, canActivate: [AuthGuard],
                resolve: {
                    data: BrandsResolver
                }
            },
            {path: 'brands/new', component: BrandsCreateComponent, canActivate: [AuthGuard]},
            {
                path: 'brands/edit/:id', component: BrandsEditComponent, canActivate: [AuthGuard],
                resolve: {
                    data: BrandEditResolver
                }
            },
            {
                path: 'car_brands', component: CarBrandsComponent, canActivate: [AuthGuard],
                resolve: {
                    data: CarBrandsResolver
                }
            },
            {path: 'car_brands/new', component: CarBrandsCreateComponent, canActivate: [AuthGuard]},
            {
                path: 'car_brands/edit/:id', component: CarBrandsEditComponent, canActivate: [AuthGuard],
                resolve: {
                    data: CarBrandEditResolver
                }
            },
            {
                path: 'tariffs', component: TariffsComponent, canActivate: [AuthGuard],
                resolve: {
                    data: TariffsResolver
                }
            },
            {
                path: 'map_zones', component: MapZonesComponent, canActivate: [AuthGuard],
                resolve: {
                    data: AreasResolver,
                }
            },
            {
                path: 'tariffs/new', component: TariffCreateComponent, canActivate: [AuthGuard]
            },
            {
                path: 'tariffs/edit/:id', component: TariffEditComponent, canActivate: [AuthGuard],
                resolve: {
                    data: TariffEditResolver
                }
            },
            {
                path: 'client_types', component: ClientTypesComponent, canActivate: [AuthGuard],
                resolve: {
                    data: ClientTypesResolver
                }
            },
            {path: 'client_types/new', component: ClientTypeCreateComponent, canActivate: [AuthGuard]},
            {
                path: 'client_types/edit/:id', component: ClientTypeEditComponent, canActivate: [AuthGuard],
                resolve: {
                    data: ClientTypeEditResolver
                }
            },
        ]
    },
    {
        path: 'terminal',
        runGuardsAndResolvers: 'always',
        component: TerminalComponent,
        resolve: {
            options : OptionsResolver,
            cars: TerminalResolver
        }
    },
    {
        path: 'terminal/terminal_print/:id',
        component: TerminalPrintComponent
    },
    {path: 'login', component: LoginComponent},
    {
        path: 'parking', component: ParkingComponent, canActivate: [AuthGuard],
        children: [
            {
                path: '', component: ParkingsComponent, canActivate: [AuthGuard], resolve: {
                    data: ParkingResolver
                }
            },
            {path: 'entry', component: ParkingEntryComponent, canActivate: [AuthGuard]},
            {path: 'exit', component: ParkingExitComponent, canActivate: [AuthGuard]}
        ]
    },
    // otherwise redirect to home
    {path: '**', redirectTo: ''}
];

export const routing = RouterModule.forRoot(appRoutes, {onSameUrlNavigation: 'reload'});
